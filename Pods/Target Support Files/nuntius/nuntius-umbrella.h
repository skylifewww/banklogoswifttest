#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "IRAEADInfo.h"
#import "IRConstants.h"
#import "IRCurve25519KeyPair.h"
#import "IRDoubleRatchetService.h"
#import "IREncryptionService.h"
#import "IRRatchetHeader.h"
#import "IRTripleDHService.h"
#import "nuntius.h"

FOUNDATION_EXPORT double nuntiusVersionNumber;
FOUNDATION_EXPORT const unsigned char nuntiusVersionString[];

