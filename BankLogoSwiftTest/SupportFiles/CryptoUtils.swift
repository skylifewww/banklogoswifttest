//
//  CryptoUtils.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 21.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import Foundation
import nuntius
import CryptoSwift
import Alamofire
import SwiftyJSON

class CryptoUtils{
    
    
    public func stringToByteArray(string: String) -> Array<UInt8>
    {
        let buf: [UInt8] = Array(string.utf8)
        return buf
    }
    
    public func prepareAuthMessage(login: String, clientNonce: String, serverNonce: String, salt: String, iterations: Int) -> String{
        let authMessage = "\(login)," + "\(clientNonce)" + "\(serverNonce)," + "\(salt)," + "\(iterations)"
        return authMessage
    }

    
   public func getSecret(secret: NSString, pin: NSString) -> NSString? {
        
        let chars = (0..<secret.length).map({
            secret.character(at: $0) ^ pin.character(at: $0 % pin.length)
        })
        return NSString(characters: chars, length: chars.count)
    }
    
    public func calculateRFC2104HMAC(saltedPassword: [UInt8], salt:String) -> [UInt8]?{

        let decodedSalt: Array<UInt8> = (salt.bytes)
        let key:[UInt8]? = try? HMAC(key: decodedSalt, variant: .sha1).authenticate(saltedPassword)
        
        return key
    }
    
    public func byteArrayXOR(leftArray: [UInt8], rightArray: [UInt8]) -> [UInt8] {
        
        var clientProofData = [UInt8]()

        for (index, item) in leftArray.enumerated() {
            clientProofData.append(item ^ rightArray[index])
        }
        return clientProofData
    }
    
    public func setSecret(leftArray: [UInt8], rightArray: [UInt8]) -> [UInt8] {
        
        var secretKey = [UInt8]()

        for (index, item) in leftArray.enumerated() {
            secretKey.append(item ^ rightArray[index])
            
        }
        return secretKey
    }
    
    public func prepareProof(authMessage: String, saltedPassword: [UInt8]) -> String {

        let clientKey:[UInt8] = calculateRFC2104HMAC(saltedPassword: saltedPassword, salt: Constants.clientKey)!
        let storedKey = clientKey.sha1()
        let clientSignature:[UInt8] = calculateRFC2104HMAC(saltedPassword:storedKey,  salt: authMessage)!
        let clientProof = byteArrayXOR(leftArray: clientKey, rightArray: clientSignature)
        print("clientProofTest: \(clientProof)")
        let clientProofSha1String:String = clientProof.toBase64()!

        return clientProofSha1String
    }
    
    public func prepareSaltedPassword(salt:String, iterations:Int, pinHash:[UInt8], secretKey:[UInt8]) -> [UInt8]{
        let saltData = Data(base64Encoded: salt, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)  
        let decodedSalt = Array<UInt8>(hex: (saltData?.hexEncodedString())!)

        let key = try? PKCS5.PBKDF2(password: secretKey, salt: decodedSalt, iterations: 4096, variant: .sha1).calculate()
        return key!
    }
    
    
    //    private func generateClientNonceHelper() -> String? {
    //
    //        var clientNonce:String?
    //
    //        var keyData = Data(count: 64)
    //        let result = keyData.withUnsafeMutableBytes {
    //            SecRandomCopyBytes(kSecRandomDefault, 64, $0)
    //        }
    //        if result == errSecSuccess {
    //            let str = keyData.base64EncodedString()
    //
    //            if str.contains("/"){
    //                generateClientNonceHelper()
    //            } else {
    //              clientNonce = str
    //            }
    //
    //        } else {
    //            print("Problem generating random bytes")
    //            return nil
    //        }
    //    }
    //
    //    public func generateClientNonce() -> String? {
    //
    //        var keyData = Data(count: 64)
    //        let result = keyData.withUnsafeMutableBytes {
    //            SecRandomCopyBytes(kSecRandomDefault, 64, $0)
    //        }
    //        if result == errSecSuccess {
    //            return keyData.base64EncodedString()
    //        } else {
    //            print("Problem generating random bytes")
    //            return nil
    //        }
    //    }
}
