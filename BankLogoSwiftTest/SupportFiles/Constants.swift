//
//  Constants.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 14.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import DPLocalization

enum BiometricSupport:Int {
    case NoBiometric = 1
    case FaceID = 2
    case TouchID = 3
}

enum Language:String {
    case EN = "en"
    case RU = "ru"
}

struct Constants {
    
    static let baseURLAuthRelease = "https://gateways-auth.release.3adigital.ru/api/v1/"
    static let baseURLAuthRed = "https://gateways-auth.red.3adigital.ru/api/v1/"
    static let baseURL = baseURLAuthRed
    static let registrationURL = baseURL + "registration"
    static let identitiesURL = baseURL + "identities/dh"
    static let sessionsURL = baseURL + "sessions"
    static let logonDeviceURL = baseURL + "logon/scram/Device/"
    static let logonScramURL = baseURL + "logon/scram"
    static let logOutURL = baseURL + "sessions"
    static let termsURL = baseURL + "https://telegram.org/privacy"
    static let bankLogoAccount = "BankLogo"
    static let keyLogin = "login"
    static let keyIsAuth = "isAuth"
    static let keyTelNumber = "telNumber"
    static let storedKey = "storedKey"
    static let clientKey = "Client Key"
    static let clientKeyPair = "clientKeyPair"
    static let keyBiometric = "keyBiometric"
    static let keyLanguage = "keyLanguage"
    static let keyTouchIDEnabled = "keyTouchIDEnabled"
    static let keyFaceIDEnabled = "keyFaceIDEnabled"
    
    static let transferDeny = "Перевод отменен"
    static let transferOk = "Перевод подтвержден"
    
    static let cellPadding: CGFloat = 5.0
    static let screenHeight: CGFloat = UIScreen.main.bounds.height
    static let screenWidth: CGFloat = UIScreen.main.bounds.width
    
    static let mainColor = UIColor.init(red: 0.194, green: 0.164, blue: 0.118, alpha: 1.0)
    
    static let noInternetOverlayTag: Int = 2000789
    //    static let dateTimeFormat = "yyyy-MM-dd HH:mm:ss"
    //    static let nonTrendedDateTimeFormat = "0000-00-00 00:00:00"
    
    // adjust here
    static let searchResultsLimit: Int = 10
    static let preferredSearchRating = "pg"
    static let preferredImageType = "fixed_width_downsampled"
    static let fullImageType = "downsized_medium"
    static let trendedIconName = "trendedIcon"
    static let trendedIconSize: CGFloat = 15
    
}


