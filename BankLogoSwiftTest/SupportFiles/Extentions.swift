//
//  Extentions.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 14.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

extension Array where Element: BinaryInteger, Element.IntegerLiteralType == UInt8 {
    public init(hex: String) {
        self.init()
        
        let utf8 = Array<Element.IntegerLiteralType>(hex.utf8)
        let skip0x = hex.hasPrefix("0x") ? 2 : 0
        for idx in stride(from: utf8.startIndex.advanced(by: skip0x), to: utf8.endIndex, by: utf8.startIndex.advanced(by: 2)) {
            let byteHex = "\(UnicodeScalar(utf8[idx]))\(UnicodeScalar(utf8[idx.advanced(by: 1)]))"
            if let byte = UInt8(byteHex, radix: 16) {
                self.append(byte as! Element)
            }
        }
    }
}

extension String {
    
    func prepareAttributedString(lineSpacing:CGFloat, alignment: NSTextAlignment) -> NSMutableAttributedString{
        let attributedString = NSMutableAttributedString(string: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        paragraphStyle.lineSpacing = lineSpacing
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        return attributedString
    }
    
    
    var hexaBytes: [UInt8] {
        var position = startIndex
        return (0..<count/2).compactMap { _ in
            defer { position = index(position, offsetBy: 2) }
            return UInt8(self[position...index(after: position)], radix: 16)
        }
    }
    var hexaData: Data { return hexaBytes.data }
}

extension Collection where Iterator.Element == UInt8 {
    var data: Data {
        return Data(self)
    }
    var hexa: String {
        return map{ String(format: "%02X", $0) }.joined()
    }
}


extension Data {
    
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
    
    var bytes : [UInt8]{
        return [UInt8](self)
    }
    
    public mutating func xor(key: Data) {
        for i in 0..<self.count {
            self[i] ^= key[i % key.count]
        }
    }
    
    
    public func checkSum() -> Int {
        return self.map { Int($0) }.reduce(0, +) & 0xff
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }
        
        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
}

extension UIViewController {
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}

extension UIViewController {
    // MARK: AlertController
    func alertControllerWithMessage(_ message: String) -> UIAlertController {
        
        let alertController = UIAlertController.init(title: "GalperinError", message: message, preferredStyle: .alert)
        let confirm = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(confirm)
        return alertController
        
    }
}

extension UIColor {
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgb & 0xFF00) >> 8) / 255.0, blue: CGFloat(rgb & 0xFF) / 255.0, alpha: alpha)
    }
}

extension UITextField {
    
    func setRoundBorderEdgeTextField(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor) {
        
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
}


extension UIButton {
    
    func setRoundBorderEdgeButton(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor) {
        
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundButton(cornerRadius: CGFloat) {
        
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    
    func setRoundedShadowButton(cornerRadius: CGFloat, color:UIColor) {
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = cornerRadius
//        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOffset = .zero
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
}

extension UILabel {
    
    func setRoundBorderEdgeLabel(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor) {
        
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
}

extension UIImageView {
    
    func setRoundedImageView(cornerRadius: CGFloat) {
        
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
}

extension UIView {
    
    func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func removeBlurEffect() {
        for subview in self.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
    
    
    func roundTop(radius:CGFloat){
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    func roundBottom(radius:CGFloat){
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    func drawCircle(radius: CGFloat, strokeColor: UIColor, lineWidth: CGFloat) {
        
        let hw = self.bounds.width/2 - radius
        
        let circlePath = UIBezierPath(ovalIn: self.bounds.insetBy(dx: hw,dy: hw) )
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        
        self.layer.addSublayer(shapeLayer)
        
    }
    
    func drawLine(start p0: CGPoint, end p1: CGPoint, strokeColor: UIColor, lineWidth: CGFloat) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, strokeColor: UIColor, lineWidth: CGFloat, lineDashPattern: [NSNumber]) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineDashPattern = lineDashPattern // 7 is the length of dash, 5 is length of the gap.
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawDottedRoundedView(cornerRadius: CGFloat, strokeColor: UIColor, lineWidth: CGFloat, lineDashPattern: [NSNumber]) {
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineDashPattern = lineDashPattern // 7 is the length of dash, 5 is length of the gap.
        
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: [UIRectCorner.topRight, UIRectCorner.topLeft, UIRectCorner.bottomRight, UIRectCorner.bottomLeft],
                                cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }

        func animateTo(frame: CGRect, withDuration duration: TimeInterval, completion: ((Bool) -> Void)? = nil) {
            guard let _ = superview else {
                return
            }
            
            let xScale = frame.size.width / self.frame.size.width
            let yScale = frame.size.height / self.frame.size.height
            let x = frame.origin.x + (self.frame.width * xScale) * self.layer.anchorPoint.x
            let y = frame.origin.y + (self.frame.height * yScale) * self.layer.anchorPoint.y
            
            UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: {
                self.layer.position = CGPoint(x: x, y: y)
                self.transform = self.transform.scaledBy(x: xScale, y: yScale)
            }, completion: completion)
        }

    
    func setupGradient(topColor: UIColor, bottomtColor: UIColor) {
        
        let gradient = CAGradientLayer()
        gradient.colors = [topColor.cgColor, bottomtColor.cgColor];
        gradient.frame = self.bounds;
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func setRoundedShadowView(cornerRadius: CGFloat, color:UIColor) {
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = cornerRadius
        self.layer.shadowOffset = CGSize(width: 0.0, height: cornerRadius + 3)
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
    
    func setRoundedShadowView(cornerRadius: CGFloat, color:UIColor, shadowOffset:CGSize) {
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = cornerRadius
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
    
    func setRoundShadow(shadowRadius: CGFloat, shadowOpacity: Float, color:UIColor) {
        
        self.layer.shadowColor = color.cgColor        
        self.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpacity
    }
    
    func setRoundedView(cornerRadius: CGFloat) {
        
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundedNoClipsToBoundsView(cornerRadius: CGFloat) {
        
        self.layer.cornerRadius = cornerRadius
//        self.layer.masksToBounds = true
//        self.clipsToBounds = true
    }
    
    func setRoundBorderEdgeViewWithAlpha(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor, alpha: CGFloat) {
        
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.withAlphaComponent(alpha).cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundBorderEdgeView(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor) {
        
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setTriangleView(){
        
        let mask = CAShapeLayer()
        mask.frame = self.layer.bounds

        let path = CGMutablePath()
        
        path.move(to: CGPoint(x: mask.frame.minX, y: mask.frame.maxY))
        path.addLine(to: CGPoint(x: mask.frame.midX, y: mask.frame.minY))
        path.addLine(to: CGPoint(x: mask.frame.maxX, y: mask.frame.maxY))
        path.addLine(to: CGPoint(x: mask.frame.minX, y: mask.frame.maxY))
     
        mask.path = path
        
        // CGPathRelease(path); - not needed
        
        self.layer.mask = mask
        
        let shape = CAShapeLayer()
        shape.frame = self.bounds
        shape.path = path
        shape.lineWidth = 1.0
        shape.strokeColor = UIColor.white.cgColor
        shape.fillColor = UIColor.clear.cgColor
        
        self.layer.insertSublayer(shape, at: 0)
    }
    
    func setTriangleRoundedView(radius:CGFloat){
        
        let mask = CAShapeLayer()
        mask.frame = self.layer.bounds
        
        
        let point1 = CGPoint(x: mask.frame.midX, y: mask.frame.minY)
        let point2 = CGPoint(x: mask.frame.maxX, y: mask.frame.maxY)
        let point3 = CGPoint(x: mask.frame.minX, y: mask.frame.maxY)
        
        let path = CGMutablePath()
        path.move(to: CGPoint(x: mask.frame.minX, y: mask.frame.maxY))
        path.addArc(tangent1End: point1, tangent2End: point2, radius: radius)
        path.addArc(tangent1End: point2, tangent2End: point3, radius: radius)
        path.addArc(tangent1End: point3, tangent2End: point1, radius: radius)
        path.closeSubpath()
        
        
        mask.path = path
        
        // CGPathRelease(path); - not needed
        
        self.layer.mask = mask
        
        let shape = CAShapeLayer()
        shape.frame = self.bounds
        shape.path = path
//        shape.lineWidth = 3.0
        let color:UIColor = #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 0.6033958014)
        shape.strokeColor = color.cgColor
        shape.fillColor = UIColor.clear.cgColor
        shape.lineWidth = 1.0
        mask.shadowColor = color.cgColor
        mask.shadowOffset = CGSize(width: 0.0, height: 3.0)
        mask.masksToBounds = false
        mask.shadowRadius = 3.0
        mask.shadowOpacity = 0.8
        
        self.layer.insertSublayer(shape, at: 0)
    }
    
    func drawTriangleRoundedLine(radius: CGFloat, strokeColor: UIColor, lineWidth: CGFloat) {
        
        let mask = CAShapeLayer()
        mask.frame = self.layer.bounds
        
        
        let point1 = CGPoint(x: mask.frame.midX, y: mask.frame.minY)
        let point2 = CGPoint(x: mask.frame.maxX, y: mask.frame.maxY)
        let point3 = CGPoint(x: mask.frame.minX, y: mask.frame.maxY)
        
        let path = CGMutablePath()
        path.move(to: CGPoint(x: mask.frame.minX, y: mask.frame.maxY))
        path.addArc(tangent1End: point1, tangent2End: point2, radius: radius)
        path.addArc(tangent1End: point2, tangent2End: point3, radius: radius)
        path.addArc(tangent1End: point3, tangent2End: point1, radius: radius)
        path.closeSubpath()
        
        
        mask.path = path
        
        // CGPathRelease(path); - not needed
        
        self.layer.mask = mask
        
        let shape = CAShapeLayer()
        shape.frame = self.bounds
        shape.path = path
        shape.lineWidth = lineWidth
        shape.strokeColor = strokeColor.cgColor
        shape.fillColor = UIColor.clear.cgColor
//        self.layer.masksToBounds = false
        
        self.layer.insertSublayer(shape, at: 0)
        
    }
    
    func roundedTriangle(radius: CGFloat, strokeColor: UIColor, lineWidth: CGFloat) {
        
        let point1 = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
        let point2 = CGPoint(x: 0, y: self.bounds.height / 2)
        let point3 = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
        
        let path = CGMutablePath()
        path.move(to: CGPoint(x: 0, y: self.bounds.height / 2))
        path.addArc(tangent1End: point1, tangent2End: point2, radius: radius)
        path.addArc(tangent1End: point2, tangent2End: point3, radius: radius)
        path.addArc(tangent1End: point3, tangent2End: point1, radius: radius)
        path.closeSubpath()
        
        let triangle = CAShapeLayer()
        triangle.fillColor = UIColor.clear.cgColor
        triangle.strokeColor = strokeColor.cgColor
        triangle.lineWidth = lineWidth
        triangle.path = path
        triangle.position = self.center
        
        self.layer.addSublayer(triangle)
        
    }
    
   
    
    func setCellShadow() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 1.0
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.layer.cornerRadius = 15
    }
    
    func setAnchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?,
                   bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,
                   paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat,
                   paddingRight: CGFloat, width: CGFloat = 0, height: CGFloat = 0) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
        }
        
        if let right = right {
            self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0 {
            self.widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            self.heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    var safeTopAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.topAnchor
        }
        return topAnchor
    }
    
    var safeLeftAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.leftAnchor
        }
        return leftAnchor
    }
    
    var safeBottomAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.bottomAnchor
        }
        return bottomAnchor
    }
    
    var safeRightAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.rightAnchor
        }
        return rightAnchor
    }
    
}
extension UIBezierPath {

    public func addArc(from startPoint: CGPoint,
                       to endPoint: CGPoint,
                       centerPoint: CGPoint,
                       radius: CGFloat,
                       clockwise: Bool) {
        let fromAngle = atan2(centerPoint.y - startPoint.y,
                              centerPoint.x - startPoint.x)
        let toAngle = atan2(endPoint.y - centerPoint.y,
                            endPoint.x - centerPoint.x)
        
        let fromOffset = CGVector(dx: -sin(fromAngle) * radius,
                                  dy: cos(fromAngle) * radius)
        let toOffset = CGVector(dx: -sin(toAngle) * radius,
                                dy: cos(toAngle) * radius)
        
        let x1 = startPoint.x + fromOffset.dx
        let y1 = startPoint.y + fromOffset.dy
        
        let x2 = centerPoint.x + fromOffset.dx
        let y2 = centerPoint.y + fromOffset.dy
        
        let x3 = centerPoint.x + toOffset.dx
        let y3 = centerPoint.y + toOffset.dy
        
        let x4 = endPoint.x + toOffset.dx
        let y4 = endPoint.y + toOffset.dy
        
        let intersectionX =
            ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4))
                / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
        let intersectionY =
            ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4))
                / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
        
        let intersection = CGPoint(x: intersectionX, y: intersectionY)
        let startAngle = fromAngle - .pi / 2.0
        let endAngle = toAngle - .pi / 2.0
        
        addArc(withCenter: intersection,
               radius: radius,
               startAngle: startAngle,
               endAngle: endAngle,
               clockwise: clockwise)
    }
}

