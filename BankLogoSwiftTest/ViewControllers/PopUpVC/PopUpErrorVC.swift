//
//  PopUpErrorVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 10.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class PopUpErrorVC: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
// let topView: UIView!
//let topImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var describLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.alpha = 0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let height = containerView.bounds.height
        let width = containerView.bounds.width
        let xPosition = containerView.frame.origin.x
        let yPosition = containerView.frame.origin.y - view.bounds.height/2
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.containerView.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
            self.view.alpha = 1
        }) { (end) in
            
        }
    }
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.view.alpha = 0
        }) { (end) in
            self.dismiss(animated: false) {
                print("dismiss")
            }
        }
    }
    
    private func setupView() {
     
       
        
        containerView.setRoundedNoClipsToBoundsView(cornerRadius: 12)
        okButton.setRoundBorderEdgeButton(cornerRadius: 12, borderWidth: 1, borderColor: #colorLiteral(red: 0.3527560234, green: 0.6400019526, blue: 0.9096098542, alpha: 1))
        self.view.layoutIfNeeded()
        prepareTriangleError()
        
        let string = """
            Здесь краткий текст, описывающий
            ошибку и рекомендации по дальшейшим
            действиям пользователя
            """
        describLabel.attributedText = string.prepareAttributedString(lineSpacing: 6, alignment: NSTextAlignment.center)
    }
    
    private func prepareCircleDenyError(){
        
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: 82, height: 82))
        topView.backgroundColor = .white
        containerView.addSubview(topView)
        let topImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        topView.addSubview(topImageView)
        
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        topView.centerYAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 82).isActive = true
        topView.widthAnchor.constraint(equalToConstant: 82).isActive = true
        
        topImageView.translatesAutoresizingMaskIntoConstraints = false
        topImageView.centerXAnchor.constraint(equalTo: (topView.centerXAnchor)).isActive = true
        topImageView.centerYAnchor.constraint(equalTo: (topView.centerYAnchor)).isActive = true
        topImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        topImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        topImageView.image = #imageLiteral(resourceName: "deny_red")
        
        topView.setRoundedView(cornerRadius: (topView.bounds.width)/2)
        topView.setRoundShadow(shadowRadius: 3, shadowOpacity: 0.8, color: #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 1))
        topView.drawCircle(radius: 34, strokeColor: #colorLiteral(red: 0.9215686275, green: 0.168627451, blue: 0.2705882353, alpha: 1), lineWidth: 1)
    }
    
    private func prepareTriangleError(){
        
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: 96, height: 86))
        topView.backgroundColor = .white
        containerView.addSubview(topView)
       
        
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        topView.centerYAnchor.constraint(equalTo: containerView.topAnchor, constant:-10).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 86).isActive = true
        topView.widthAnchor.constraint(equalToConstant: 96).isActive = true
        topView.layoutIfNeeded()
        let topImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 9, height: 38))
        topView.addSubview(topImageView)
        topImageView.translatesAutoresizingMaskIntoConstraints = false
        topImageView.centerXAnchor.constraint(equalTo: (topView.centerXAnchor)).isActive = true
        topImageView.centerYAnchor.constraint(equalTo: (topView.bottomAnchor), constant: -32).isActive = true
        topImageView.heightAnchor.constraint(equalToConstant: 38).isActive = true
        topImageView.widthAnchor.constraint(equalToConstant: 9).isActive = true
        
        topImageView.layoutIfNeeded()
        
        let secondTriangle = UIView(frame: CGRect(x: 0, y: 0, width: 72, height: 66))
        topView.addSubview(secondTriangle)
        secondTriangle.backgroundColor = .clear
        secondTriangle.translatesAutoresizingMaskIntoConstraints = false
        secondTriangle.centerXAnchor.constraint(equalTo: (topView.centerXAnchor)).isActive = true
        secondTriangle.centerYAnchor.constraint(equalTo: (topView.centerYAnchor), constant: 4).isActive = true
        secondTriangle.heightAnchor.constraint(equalToConstant: 66).isActive = true
        secondTriangle.widthAnchor.constraint(equalToConstant: 72).isActive = true
        
        
        
        topImageView.image = #imageLiteral(resourceName: "notice_small")

        topView.setTriangleRoundedView(radius: 9)
        secondTriangle.drawTriangleRoundedLine(radius: 4, strokeColor: #colorLiteral(red: 0.9490196078, green: 0.7058823529, blue: 0.2196078431, alpha: 1), lineWidth: 2.8)
        topView.setRoundShadow(shadowRadius: 3, shadowOpacity: 0.6, color: #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 1))
        secondTriangle.layoutIfNeeded()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
