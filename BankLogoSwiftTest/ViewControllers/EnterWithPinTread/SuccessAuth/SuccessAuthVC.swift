//
//  SuccessAuthVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 26.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class SuccessAuthVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var roundedSuccessView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

       setupViews()
    }
    
    //MARK:- IBAction Metods
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        print("continueButtonTapped!!!!!!!!!!!!!!!!!!!!!!!!!")
        goToFunctionsVC()
    }
    
    private func goToFunctionsVC() {
        
        let functionsVC = self.storyboard?.instantiateViewController(withIdentifier: "FunctionsVC") as! FunctionsVC
        
        self.presentDetail(functionsVC)
    }
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")

        } else {
            
        }

        
        //MARK:- Setup Views
        
//        titleLabel.text = """
//        Теперь ты можешь включить
//        лимиты в Telegram и настроить
//        бесконтактные платежи
//        прямо из приложения
//        """

        continueButton.setRoundButton(cornerRadius: 12)
        roundedSuccessView.setRoundBorderEdgeView(cornerRadius: 32, borderWidth: 1, borderColor: #colorLiteral(red: 0.5098039216, green: 0.7764705882, blue: 0.3176470588, alpha: 1))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
