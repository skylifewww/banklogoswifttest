//
//  EnterSMSCodeVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 15.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import nuntius
import CryptoSwift
import Alamofire
import SwiftyJSON
import Locksmith

private let reuseIdentifierNumPadCell = "NumPadCell"

class EnterSMSCodeVC: UIViewController, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var enterSMSLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var repeatImageView: UIImageView!
    @IBOutlet weak var repeatBorderView: UIView!
    @IBOutlet weak var textField1: UIView!
    @IBOutlet weak var textField2: UIView!
    @IBOutlet weak var textField3: UIView!
    @IBOutlet weak var textField4: UIView!
    
    @IBOutlet weak var cursourView1: UIView!
    @IBOutlet weak var cursourView2: UIView!
    @IBOutlet weak var cursourView3: UIView!
    @IBOutlet weak var cursourView4: UIView!
    
    @IBOutlet weak var codeLabel1: UILabel!
    @IBOutlet weak var codeLabel2: UILabel!
    @IBOutlet weak var codeLabel3: UILabel!
    @IBOutlet weak var codeLabel4: UILabel!
    
    @IBOutlet weak var numberPudCollectionView: UICollectionView!
    @IBOutlet weak var numPadBottom: NSLayoutConstraint!
    @IBOutlet weak var numPadHeight: NSLayoutConstraint!
    @IBOutlet weak var numPadWidth: NSLayoutConstraint!
    @IBOutlet weak var repeatSMSLabel: UILabel!
    @IBOutlet weak var borderActivityView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var errorSMSLabel: UILabel!
    
    var isRepeating = false
    let arrNumPad = ["1", "2", "3", "4", "5", "6", "7", "8", "9", " ", "0", "backspace"]
    var number = ""
    var cursor:UIView?
    var pinHash: [UInt8] = [UInt8]()
    
    
    var confirmationToken:String? {
        didSet {
            print("confirmationToken did set " + confirmationToken!)
            }
    }
    
    var numberTel:String? {
        didSet {
            print("numberTel did set " + numberTel!)
        }
    }
    
    var numberTelStr:String? {
        didSet {
            print("numberTel did set " + numberTelStr!)
        }
    }
    
    let cryptoUtils = CryptoUtils()
    let networkManager = NetworkManager()
    var login:String? {
        didSet {
            print("login did set " + login!)
        }
    }

    var serverOpenKey:String? {
        didSet {
            print("serverOpenKey did set " + serverOpenKey!)
        }
    }
    var clientKeyPair:AxlSign.Keys?
    var clientProof:String? {
        didSet {
            print("clientProof did set " + clientProof!)
        }
    }
    var clientNonce:String? {
        didSet {
            print("clientNonce did set " + clientNonce!)
        }
    }
    var serverNonce:String? {
    didSet {
    print("serverNonce did set " + serverNonce!)
      }
    }
    var getSecretKey:String? {
    didSet {
    print("getSecretKey did set " + getSecretKey!)
      }
    }
    var salt:String? {
        didSet {
            print("salt did set " + salt!)
        }
    }
    
    var iterations = 4096
    var saltedPassword:[UInt8]?
    var authMessage:String? {
    didSet {
    print("authMessage did set " + authMessage!)
      }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()

    }
    
    //MARK:- IBAction Metods
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        cursor?.layer.removeAllAnimations()
        cursor = nil
        self.dismissDetail()
    }
    
    @IBAction func repeatButtonTapped(_ sender: UIButton) {
        
        isRepeating = !isRepeating
        repeatUISetup()
        errorSMSLabel.isHidden = true
        codeLabel1.text = ""
        codeLabel2.text = ""
        codeLabel3.text = ""
        codeLabel4.text = ""
        
        cursourView1.alpha = 0
        cursourView2.alpha = 0
        cursourView3.alpha = 0
        cursourView4.alpha = 0
        
        cursor = cursourView1
    }
    
    private func repeatUISetup(){
        if isRepeating {
            repeatSMSLabel.text =
            """
            Повторить SMS
            через 15 сек
            """
            repeatSMSLabel.textColor = #colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)
            
            repeatBorderView.setRoundBorderEdgeView(cornerRadius: 23, borderWidth: 1, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
            repeatBorderView.backgroundColor = .white
            repeatImageView.image = #imageLiteral(resourceName: "repeat")
            
            
        } else {
            repeatSMSLabel.text =
            """
            Повторить
            SMS
            """
            repeatSMSLabel.textColor = #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 1)
            
            repeatBorderView.setRoundedView(cornerRadius: 23)
            repeatBorderView.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            repeatImageView.image = #imageLiteral(resourceName: "repeat_white")
        }
    }
    
    private func activityIndicatorSetup(){
        borderActivityView.isHidden = false
        borderActivityView.setRoundBorderEdgeView(cornerRadius: 23, borderWidth: 1, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        repeatBorderView.isHidden = true
        repeatSMSLabel.isHidden = true
    }


    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")
            numPadBottom.constant = 0
            numPadHeight.constant = 240
            
        } else if (Device.IS_IPHONE_X){
            numPadBottom.constant = 16
        } else {
            numPadBottom.constant = -6
        }
        
        //MARK:- UICollectionViewFlowLayout
        let nibName = UINib(nibName: reuseIdentifierNumPadCell, bundle: nil)
        
        numberPudCollectionView.register(nibName, forCellWithReuseIdentifier: reuseIdentifierNumPadCell)
        
        let numItem = 3
        let widthArea = numberPudCollectionView.bounds.size.width - 2
        let heightArea = (UIScreen.main.bounds.size.height * 0.4) - 4
        let widthItem = floor(widthArea / CGFloat(numItem))
        let heightItem = heightArea / 4

        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: widthItem, height: heightItem)
        numberPudCollectionView.collectionViewLayout = layout
        
        //MARK:- Setup Views
        
        let numStr = String((numberTelStr?.prefix(5))!) + "*)***-**" + String((numberTelStr?.suffix(2))!)
        print(numStr)
        enterSMSLabel.text = """
        Введи код из SMS, которую
        мы отправили на номер
        \(numStr)
        """
        
        errorSMSLabel.isHidden = true
        borderActivityView.isHidden = true
        
        repeatUISetup()

        textField1.setRoundBorderEdgeView(cornerRadius: 6, borderWidth: 1, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        textField2.setRoundBorderEdgeView(cornerRadius: 6, borderWidth: 1, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        textField3.setRoundBorderEdgeView(cornerRadius: 6, borderWidth: 1, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        textField4.setRoundBorderEdgeView(cornerRadius: 6, borderWidth: 1, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        
        codeLabel1.text = ""
        codeLabel2.text = ""
        codeLabel3.text = ""
        codeLabel4.text = ""
        
        cursourView1.alpha = 0
        cursourView2.alpha = 0
        cursourView3.alpha = 0
        cursourView4.alpha = 0
        
        cursor = cursourView1
        self.fadeOut(finished: true)

    }
    
    //MARK:- Cursor Animations Metods
    private func fadeIn(finished: Bool) {
        if let cursor = self.cursor{
            UIView.animate(withDuration:0.5, delay: 0, options: [.curveEaseOut], animations: { cursor.alpha = 1 } , completion: self.fadeOut)
        }
    }
    private func fadeOut(finished: Bool) {
        if let cursor = self.cursor{
            UIView.animate(withDuration:0.5, delay: 0, options: [.curveEaseOut], animations: { cursor.alpha = 0 } , completion: self.fadeIn)
        }
    }
    
//MARK:- Memory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func goToEnterPIN1VC(confirmationToken: String, numberTel:String) {
        let enterPin1VC = self.storyboard?.instantiateViewController(withIdentifier: "EnterPin1VC") as! EnterPin1VC
        enterPin1VC.confirmationToken = confirmationToken
        enterPin1VC.numberTel = numberTel
        self.presentDetail(enterPin1VC)
    }
    
    private func goToNotClientVC() {
        let notClientVC = self.storyboard?.instantiateViewController(withIdentifier: "NotClientVC") as! NotClientVC
        self.presentDetail(notClientVC)
    }

}

//MARK:- UICollectionViewDataSource
extension EnterSMSCodeVC:UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierNumPadCell, for: indexPath) as! NumPadCell
        
        cell.setupWithData(data: arrNumPad[indexPath.item])
        return cell
    }
}

//MARK:- UICollectionViewDelegate
extension EnterSMSCodeVC:UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = arrNumPad[indexPath.item]
        
        switch data {
        case "backspace":

            if number.count == 1{
                errorSMSLabel.isHidden = true
                codeLabel1.text = ""
                cursor = cursourView1
                cursourView2.alpha = 0
                number = ""
                
            } else if number.count == 2{
                errorSMSLabel.isHidden = true

                codeLabel2.text = ""
                number.removeLast()
                cursourView3.alpha = 0
                cursor = cursourView2
    
            } else if number.count == 3{
                errorSMSLabel.isHidden = true

                codeLabel3.text = ""
                number.removeLast()
                cursourView4.alpha = 0
                cursor = cursourView3
                
            } else if number.count == 4{
                errorSMSLabel.isHidden = true

                codeLabel4.text = ""
                number.removeLast()
//                cursourView4.alpha = 0
                cursor = cursourView4
            }
//            print(number)

            
        case " ":

            print(data)
            
        case "1", "2", "3", "4", "5", "6", "7", "8", "9", "0":

            if number.count == 0{
                errorSMSLabel.isHidden = true
                codeLabel1.text = data
                number.append(data)
                cursourView1.alpha = 0
                cursor = cursourView2

            } else if number.count == 1{
                errorSMSLabel.isHidden = true
                codeLabel2.text = data
                number.append(data)
                cursourView2.alpha = 0
                cursor = cursourView3
                
            } else if number.count == 2{
                errorSMSLabel.isHidden = true
                codeLabel3.text = data
                number.append(data)
                cursourView3.alpha = 0
                cursor = cursourView4
            } else if number.count == 3{
                codeLabel4.text = data
                number.append(data)
                print(number)
                guard let numberTel = numberTel else {return}
                guard let confirmationToken = confirmationToken else {return}
                
                if number != "0000" {
                    number = ""
                    errorSMSLabel.isHidden = false
                    
                } else if number == "0000" {
                    
                    self.numberPudCollectionView.isHidden = true
                    activityIndicatorSetup()
                    errorSMSLabel.isHidden = true
                    
                    networkManager.putRegisterRequest(cellPhone: numberTel, confirmationToken: confirmationToken, smsCode: number) { (state, verifier, error) in
                        
                        if error == nil && state == "Complete"{
                            print("goToEnterPIN1VC")
                            self.goToEnterPIN1VC(confirmationToken: confirmationToken, numberTel: numberTel)
                            
                            print(state)
                        } else {
                            print(error!)
                            if error == "telegram_not_connected"{
                                self.activityIndicator.stopAnimating()
                                self.goToNotClientVC()
                            }
                        }
                    }
                }
                
              
            }
            
        default:
            print("default")
        }
    }
}
