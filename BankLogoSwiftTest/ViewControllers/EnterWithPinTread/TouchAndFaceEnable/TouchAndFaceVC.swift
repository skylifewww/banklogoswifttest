//
//  TouchAndFaceVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 30.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import DPLocalization
import LocalAuthentication

private let reuseIdentifierTouchAndFaceCell = "TouchAndFaceCell"

class TouchAndFaceVC: UIViewController {
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var touchTableView: UITableView!
//    let arrTitles = [DPLocalizedString("touch_id", nil), DPLocalizedString("face_id", nil)]
    var isTouchIDEnabled = false
    var isFaceIDEnabled = false
    var currentBiometric:BiometricSupport?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        print("continueButtonTapped!!!!!!!!!!!!!!!!!")
        goToSuccessVC()
    }
    
    private func goToSuccessVC() {
        let successAuthVC = self.storyboard?.instantiateViewController(withIdentifier: "SuccessAuthVC") as! SuccessAuthVC
        
        self.presentDetail(successAuthVC)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupContinueButton(){
        if isTouchIDEnabled == true || isFaceIDEnabled == true {
            print("isTouchIDEnabled == true")
            continueButton.setRoundButton(cornerRadius: 12)
            continueButton.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            continueButton.titleLabel?.textColor = .white
            continueButton.setTitleColor(.white, for: UIControlState.normal)
            continueButton.setTitle("Продолжить", for: UIControlState.normal)
        } else if isTouchIDEnabled == false && isFaceIDEnabled == false {
            print("isTouchIDEnabled == false")
            continueButton.backgroundColor = .clear
            continueButton.setTitleColor(#colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1), for: UIControlState.normal)
            continueButton.tintColor = .white
            continueButton.setTitle("Пропустить", for: UIControlState.normal)
        }
    }
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")
            
        } else {
            
        }
        
        
        //MARK:- Setup Views
        setupContinueButton()
        

    }
    
    @objc func turnOrOffCell(sender:UISwitch){
        
        switch sender.tag {
        case 0:
            print("switchControl.tag \(sender.tag)")
            if sender.isOn{
                print("sender.isOn \(sender.isOn)")
                isTouchIDEnabled = true
                if UserDefaults.standard.integer(forKey: Constants.keyBiometric) == BiometricSupport.FaceID.rawValue {
                    UserDefaults.standard.set(isTouchIDEnabled, forKey: Constants.keyFaceIDEnabled)
                } else if UserDefaults.standard.integer(forKey: Constants.keyBiometric) == BiometricSupport.TouchID.rawValue {
                    UserDefaults.standard.set(isTouchIDEnabled, forKey: Constants.keyTouchIDEnabled)
                }
                UserDefaults.standard.synchronize()
                setupContinueButton()
            } else {
                print("sender.isOn \(sender.isOn)")
                isTouchIDEnabled = false
                if UserDefaults.standard.integer(forKey: Constants.keyBiometric) == BiometricSupport.FaceID.rawValue {
                    UserDefaults.standard.set(isTouchIDEnabled, forKey: Constants.keyFaceIDEnabled)
                } else if UserDefaults.standard.integer(forKey: Constants.keyBiometric) == BiometricSupport.TouchID.rawValue {
                    UserDefaults.standard.set(isTouchIDEnabled, forKey: Constants.keyTouchIDEnabled)
                }
                UserDefaults.standard.synchronize()
                setupContinueButton()
            }
//        case 1:
//            print("switchControl.tag \(sender.tag)")
//            if sender.isOn{
//                print("sender.isOn \(sender.isOn)")
//                isFaceIDEnabled = true
//                setupContinueButton()
//            } else {
//                print("sender.isOn \(sender.isOn)")
//                isFaceIDEnabled = false
//                setupContinueButton()
//            }
        default:
            print("default")
        }
    }
}
    
    //MARK:- UITableViewDataSource
    extension TouchAndFaceVC:UITableViewDataSource
    {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTouchAndFaceCell, for: indexPath) as! TouchAndFaceCell
            if UserDefaults.standard.integer(forKey: Constants.keyBiometric) == BiometricSupport.FaceID.rawValue {
                cell.setupWithData(data: "face_id", row:indexPath.row)
            } else if UserDefaults.standard.integer(forKey: Constants.keyBiometric) == BiometricSupport.TouchID.rawValue  {
                cell.setupWithData(data: "touch_id", row:indexPath.row)
            }
            cell.switchCell.addTarget(self, action: #selector(turnOrOffCell), for: .valueChanged)
            return cell
        }
    }

    
    //MARK:- UITableViewDelegate
    extension TouchAndFaceVC:UITableViewDelegate
    {
        
        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
            print("indexPath.row \(indexPath.row)")
        }
        
}
