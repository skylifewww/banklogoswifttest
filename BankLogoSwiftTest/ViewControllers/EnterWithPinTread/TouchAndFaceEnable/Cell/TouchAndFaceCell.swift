//
//  TouchAndFaceCell.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 30.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import DPLocalization

class TouchAndFaceCell: UITableViewCell {

    @IBOutlet weak var switchCell: UISwitch!
    @IBOutlet weak var titleLabel: UILabel!
    
    var isDrawTopLine = false
    var isDrawBottomLine = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        switchCell.layer.cornerRadius = switchCell.frame.height / 2
    }
    
    func setupWithData(data: String, row:Int){
        
        if  UserDefaults.standard.bool(forKey: Constants.keyFaceIDEnabled) == true || UserDefaults.standard.bool(forKey: Constants.keyTouchIDEnabled) == true {
            switchCell.isOn = true
        } else {
            switchCell.isOn = false
        }

        titleLabel.text = DPLocalizedString(data, nil)
        isDrawTopLine = true
        isDrawBottomLine = true

//        if row == 1 {
//            isDrawBottomLine = true
//        }
        switchCell.tag = row
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        if isDrawTopLine {
            contentView.drawLine(start: CGPoint(x:titleLabel.bounds.minX, y: contentView.bounds.minY), end: CGPoint(x: contentView.bounds.maxX - 12, y: contentView.bounds.minY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        }
        if isDrawBottomLine {
            contentView.drawLine(start: CGPoint(x:titleLabel.bounds.minX, y: contentView.bounds.maxY), end: CGPoint(x: contentView.bounds.maxX - 12, y: contentView.bounds.maxY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
            
        }
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
