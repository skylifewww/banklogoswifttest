//
//  EnterPin1VC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 25.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import nuntius
import CryptoSwift
import Alamofire
import SwiftyJSON
import Locksmith

private let reuseIdentifierNumPadCell = "NumPadCell"

enum ErrorPIN:String {
    case verySimple = """
    Введенный PIN-код слишком простой,
    попробуй создать код ещё раз
    """
    
    case notEqual =
    """
    Введенные PIN-коды не совпадают,
    попробуй создать код ещё раз
    """
}

class EnterPin1VC: UIViewController, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var enterPINLabel: UILabel!
    @IBOutlet weak var dote1: UIView!
    @IBOutlet weak var dote2: UIView!
    @IBOutlet weak var dote3: UIView!
    @IBOutlet weak var dote4: UIView!
    @IBOutlet weak var numberPudCollectionView: UICollectionView!
    @IBOutlet weak var numPadBottom: NSLayoutConstraint!
    @IBOutlet weak var numPadHeight: NSLayoutConstraint!
    @IBOutlet weak var numPadWidth: NSLayoutConstraint!
    @IBOutlet weak var containerStackView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    
    let cryptoUtils = CryptoUtils()
    let networkManager = NetworkManager()
    
    let arrNumPad = ["1", "2", "3", "4", "5", "6", "7", "8", "9", " ", "0", "backspace"]
    var number = ""
    var number1 = ""
    var isSuccessPIN = false
    var isFirstStep = true
    
    var changePIN:Bool? {
        didSet {
            print("isAuth did set \(changePIN!)")
            
        }
    }
    
    var isAuth:Bool? {
        didSet {
            print("isAuth did set \(isAuth!)")
        }
    }
    
    var confirmationToken:String? {
        didSet {
            print("confirmationToken did set " + confirmationToken!)
        }
    }
    var numberTel:String? {
        didSet {
            print("numberTel did set " + numberTel!)
        }
    }
    
    var login:String? {
        didSet {
            print("login did set " + login!)
        }
    }
    
    var pin:String? {
        didSet {
            print("pin did set " + pin!)
        }
    }
    
    var serverOpenKey:String? {
        didSet {
            print("serverOpenKey did set " + serverOpenKey!)
        }
    }
    let axlsign = AxlSign()

    var clientKeyPair:AxlSign.Keys?
    var clientProof:String? {
        didSet {
            print("clientProof did set " + clientProof!)
        }
    }
    var clientNonce:String? {
        didSet {
            print("clientNonce did set " + clientNonce!)
        }
    }
    var serverNonce:String? {
        didSet {
            print("serverNonce did set " + serverNonce!)
        }
    }
    var getSecretKey:String? {
        didSet {
            print("getSecretKey did set " + getSecretKey!)
        }
    }
    var setSecretKey:[UInt8]? {
        didSet {
            print("getSecretKey did set " + (setSecretKey?.toBase64())!)
        }
    }
    
    var salt:String? {
        didSet {
            print("salt did set " + salt!)
        }
    }
    
    var sharedSecret:[UInt8]?{
        didSet {
            print("sharedSecret did set ")
        }
    }
    
    var sharedSecretSha1:[UInt8]?{
        didSet {
            print("sharedSecretSha1 did set ")
        }
    }
    
    var iterations = 4096
    var saltedPassword:[UInt8]?
    var authMessage:String? {
        didSet {
            print("authMessage did set " + authMessage!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
//    override func layoutIfNeeded() {
//        super.layoutIfNeeded()
//        containerStackView.drawLine(start: CGPoint(x:containerStackView.bounds.minX, y: containerStackView.bounds.minY), end: CGPoint(x: containerStackView.bounds.maxX, y: containerStackView.bounds.minY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
//        containerStackView.drawLine(start: CGPoint(x:containerStackView.bounds.minX, y: containerStackView.bounds.maxY), end: CGPoint(x: containerStackView.bounds.maxX, y: containerStackView.bounds.maxY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
//
//    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
//        self.dismiss(animated: true, completion: nil)
        self.dismissDetail()
    }
    
    private func goToTouchAndFaceVC() {

        let touchAndFaceVC = self.storyboard?.instantiateViewController(withIdentifier: "TouchAndFaceVC") as! TouchAndFaceVC
        
        self.presentDetail(touchAndFaceVC)
    }
    
    //MARK:- Network Metods
    
    private func prepareAuthGetScramRequest(pin: String){
        let keychainDict = Locksmith.loadDataForUserAccount(userAccount: Constants.bankLogoAccount)
        
        if let login = keychainDict![Constants.keyLogin] as? String, let storedKey = keychainDict![Constants.storedKey] as? [UInt8] {
            
            
            let pinSha1 = pin.bytes.sha1()
            sharedSecretSha1 = cryptoUtils.setSecret(leftArray: storedKey, rightArray: pinSha1)

            self.networkManager.getScrumDeviceRequest(confirmationToken: "", login: login) { (serverNonce, salt, iterations, error) in
                if error == nil {
                    self.serverNonce = serverNonce.replacingOccurrences(of: "\\/", with: "/")
                    print(serverNonce)
                    
                    self.salt = salt.replacingOccurrences(of: "\\/", with: "/")
                    print(salt)
                    
                    self.iterations = iterations
                    print(iterations)

                    self.generateClientNonce()
                    
                } else {
                    print(error!)
                }
            }
        }
    }
    
    private func preparePostScrumLoginRequest(clientNonce: String, clientProof: String, login: String, serverNonce: String){
        
        self.networkManager.postScrumLoginRequest(clientNonce: clientNonce, clientProof: clientProof, login: login, serverNonce: serverNonce, confirmationToken: confirmationToken!, completionHandler: { (state, verifier, error) in
            if error == nil && state == "Complete" {
                self.goToTouchAndFaceVC()
                print(state)
                print(verifier)
            } else {
                print(error!)
            }
        })
    }
    
    
    private func preparePostIdentitiesRequest()
    {
        if let clientPublicKey = clientKeyPair?.publicKey {
            let clientPublicKeyString:String = clientPublicKey.toBase64()!

            
            networkManager.postIdentitiesRequest(clientOpenKey: clientPublicKeyString, confirmationToken: confirmationToken!) { (login, serverOpenKey, error) in
                if error == nil && login.count > 0 && serverOpenKey.count > 0 {
                    self.login = login
                    
                   // Save login to Keychain
               
                        do {
                            try Locksmith.saveData(data: [Constants.keyLogin : login], forUserAccount: Constants.bankLogoAccount)
                        } catch {
                            print("Unable to save data")
                        }
                    // load login from Keychain
                    let keychainDict = Locksmith.loadDataForUserAccount(userAccount: Constants.bankLogoAccount)
                    
                    let login:String? = keychainDict![Constants.keyLogin] as? String
                    
                  
//                    self.networkManager.logoutRequest()
                    self.networkManager.getScrumDeviceRequest(confirmationToken: self.confirmationToken!, login: login!) { (serverNonce, salt, iterations, error) in
                        if error == nil {
                            self.serverNonce = serverNonce.replacingOccurrences(of: "\\/", with: "/")
                            print(serverNonce)
                            
                            self.salt = salt.replacingOccurrences(of: "\\/", with: "/")
                            print(salt)
                            
                            self.iterations = iterations
                            print(iterations)
                            
                            self.createSharedSecret(serverNonce: serverNonce, salt: salt, clientKeyPair: self.clientKeyPair!, serverOpenKye: serverOpenKey)
                            
                        } else {
                            print(error!)
                        }
                        
                    }
                    self.serverOpenKey = serverOpenKey
                } else {
                    print(error!)
                }
            }
        }
    }
    
    private func generateKeyPair(pin: String){
        
        let seed = axlsign.randomBytes(32)
        let keyPairTemp = axlsign.generateKeyPair(seed)
        let clientPublicKeyString:String = keyPairTemp.publicKey.toBase64()!
        
        if clientPublicKeyString.contains("/"){
            generateKeyPair(pin: pin)
        } else {
            clientKeyPair = keyPairTemp
            preparePostIdentitiesRequest()
        }
    }
    
    private func createSharedSecret(serverNonce: String, salt: String, clientKeyPair: AxlSign.Keys, serverOpenKye:String){
        
        let serverPublicKeyData = Data(base64Encoded: serverOpenKye, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)

        let serverPublicKey = Array<UInt8>(hex: (serverPublicKeyData?.hexEncodedString())!)
        sharedSecret = AxlSign.sharedInstance.sharedKey(secretKey: clientKeyPair.privateKey, publicKey: serverPublicKey)
        sharedSecretSha1 = sharedSecret?.sha1()
        let pinSha1 = pin?.bytes.sha1()
        setSecretKey = cryptoUtils.setSecret(leftArray: sharedSecretSha1!, rightArray: pinSha1!)
        
        // save storedKey to Keychain
        if  let storedKey = setSecretKey {
            do {
//                try Locksmith.saveData(data: [Constants.keyLogin : self.login!, Constants.storedKey: storedKey], forUserAccount: Constants.bankLogoAccount)
                try Locksmith.updateData(data: [Constants.keyLogin : self.login!, Constants.storedKey: storedKey], forUserAccount: Constants.bankLogoAccount)
            } catch {
                print("Unable to save data")
            }
        }
        // load storedKey from Keychain
        let keychainDict = Locksmith.loadDataForUserAccount(userAccount: Constants.bankLogoAccount)
        if keychainDict![Constants.storedKey] as? [UInt8] != nil && ((keychainDict![Constants.keyLogin] as? String) != nil){
            UserDefaults.standard.set(true, forKey: Constants.keyIsAuth)
        }

        let secret:[UInt8]? = keychainDict![Constants.storedKey] as? [UInt8]
        
        sharedSecretSha1 = cryptoUtils.setSecret(leftArray: secret!, rightArray: pinSha1!)

        generateClientNonce()
    }
    
    
    
    private func createPassword(){
        let saltedPassword = cryptoUtils.prepareSaltedPassword(salt: salt!, iterations: 4096, pinHash: (pin?.bytes)!, secretKey: sharedSecretSha1!)

        let authMessage = cryptoUtils.prepareAuthMessage(login: login!, clientNonce: clientNonce!, serverNonce: serverNonce!, salt: salt!, iterations: 4096)
        print(authMessage)
        
        let clientProof = cryptoUtils.prepareProof(authMessage: authMessage, saltedPassword: saltedPassword)

        self.preparePostScrumLoginRequest(clientNonce: clientNonce!, clientProof: clientProof, login: login!, serverNonce: serverNonce!)
    }
    
    private func generateClientNonce(){

        var keyData = Data(count: 64)
        let result = keyData.withUnsafeMutableBytes {
            SecRandomCopyBytes(kSecRandomDefault, 64, $0)
        }
        if result == errSecSuccess {
            let str = keyData.base64EncodedString()
            
            if str.contains("/"){
                generateClientNonce()
            } else {
                self.clientNonce = str
                createPassword()
            }
            
        } else {
//            print("Problem generating random bytes")
//            return nil
        }
    }
 
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")
            numPadBottom.constant = 0
            numPadHeight.constant = 240
            
        } else if (Device.IS_IPHONE_X){
            numPadBottom.constant = 16
        } else {
            numPadBottom.constant = -6
        }
        
        //MARK:- UICollectionViewFlowLayout
        let nibName = UINib(nibName: reuseIdentifierNumPadCell, bundle: nil)
        
        numberPudCollectionView.register(nibName, forCellWithReuseIdentifier: reuseIdentifierNumPadCell)
        
        let numItem = 3
        let widthArea = numberPudCollectionView.bounds.size.width - 2
        let heightArea = (UIScreen.main.bounds.size.height * 0.4) - 4
        let widthItem = floor(widthArea / CGFloat(numItem))
        let heightItem = heightArea / 4
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: widthItem, height: heightItem)
        numberPudCollectionView.collectionViewLayout = layout
        
        //MARK:- Setup Views
        
        backButton.isHidden = true
        backButton.isUserInteractionEnabled = false
        
        if changePIN! {
            backButton.isHidden = false
            backButton.isUserInteractionEnabled = true
        }
        
        
        enterPINLabel.text =
        """
        Придумай PIN-код
        
        """

        dote1.setRoundedView(cornerRadius: 5)
        dote2.setRoundedView(cornerRadius: 5)
        dote3.setRoundedView(cornerRadius: 5)
        dote4.setRoundedView(cornerRadius: 5)
        
        self.view.layoutIfNeeded()
        
        containerStackView.drawLine(start: CGPoint(x:containerStackView.bounds.minX, y: containerStackView.bounds.minY), end: CGPoint(x: containerStackView.bounds.maxX, y: containerStackView.bounds.minY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        containerStackView.drawLine(start: CGPoint(x:containerStackView.bounds.minX, y: containerStackView.bounds.maxY), end: CGPoint(x: containerStackView.bounds.maxX, y: containerStackView.bounds.maxY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        
    }
    
    private func prepareUISMSCodeError(errorPIN:ErrorPIN){
        
        enterPINLabel.text = errorPIN.rawValue

        enterPINLabel.textColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dote1.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dote2.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dote3.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dote4.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        
        isFirstStep = true
        number1 = ""
        number = ""
    }
    
    private func prepareUIFirstStep(){
        
        enterPINLabel.textColor = #colorLiteral(red: 0.2863244414, green: 0.3505810201, blue: 0.412160933, alpha: 1)
        dote1.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
        dote2.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
        dote3.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
        dote4.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
    }
    
    private func checkPinEqual(firstPIN:String, secondPIN:String) -> Bool {
        return firstPIN == secondPIN
    }
    private func checkVerySimplePin(pin:String) -> Bool {
        let simplePins:Set<String> = ["0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999"]
        return simplePins.contains(pin)
    }
}
    
//MARK:- UICollectionViewDataSource
extension EnterPin1VC:UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierNumPadCell, for: indexPath) as! NumPadCell
        
        cell.setupWithData(data: arrNumPad[indexPath.item])
        return cell
    }
}

//MARK:- UICollectionViewDelegate
extension EnterPin1VC:UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = arrNumPad[indexPath.item]
        
        switch data {
        case "backspace":
            
            if number.count == 1{
                number = ""
                dote1.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
                
            } else if number.count == 2{
                number.removeLast()
                dote2.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
                
            } else if number.count == 3{
                number.removeLast()
                dote3.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
                
            } else if number.count == 4{
                number.removeLast()
                dote4.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
            }
            print(number)
            
            
        case " ":
            
            print(data)
            
        case "1", "2", "3", "4", "5", "6", "7", "8", "9", "0":
            
            if number.count == 0{
                
                if isFirstStep {
                    enterPINLabel.text =
                    """
                    Придумай PIN-код
                    
                    """
                }
                
                prepareUIFirstStep()
                number.append(data)
                dote1.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                
            } else if number.count == 1{
                number.append(data)
                dote2.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                
            } else if number.count == 2{
                number.append(data)
                dote3.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            } else if number.count == 3{
                
                dote4.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                print(number)
                print(number1)
                
                if isFirstStep {
                    number.append(data)
                    
                    if !checkVerySimplePin(pin:number) {
                        number1 = number
                        number = ""
                        isFirstStep = false
                        prepareUIFirstStep()
                        enterPINLabel.text =
                        """
                        Повтори PIN-код ещё раз
                        
                        """
                    } else {
                        prepareUISMSCodeError(errorPIN: ErrorPIN.verySimple)
                    }
                    
                } else {
                    number.append(data)
                    if checkPinEqual(firstPIN:number, secondPIN:number1) {
                        pin = "\(number1)"
                        if isAuth! {
                            prepareAuthGetScramRequest(pin: pin!)
                        } else {
                            generateKeyPair(pin: pin!)
                        }
                        
                    } else {
                        prepareUISMSCodeError(errorPIN: ErrorPIN.notEqual)
                    }
                }
            }
            
        default:
            print("default")
        }
    }
}
