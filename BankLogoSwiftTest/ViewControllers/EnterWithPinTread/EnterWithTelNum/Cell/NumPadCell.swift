//
//  NumPadCell.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 14.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class NumPadCell: UICollectionViewCell {

    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var numImageView: UIImageView!
    @IBOutlet weak var touchImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupWithData(data: String){
        
        if data == "touchNumPad"{
            numLabel.isHidden = true
            numImageView.isHidden = true
            touchImageView.isHidden = false
            touchImageView.image = UIImage(named: data)
            
        } else  if data == "backspace" {
            numLabel.isHidden = true
            touchImageView.isHidden = true
            numImageView.isHidden = false
            numImageView.image = UIImage(named: data)
            
        } else {
            touchImageView.isHidden = true
            numLabel.isHidden = false
            numImageView.isHidden = true
            numLabel.text = data
        }
    }
    override var isHighlighted: Bool {
        willSet {
            onSelected(newValue)
        }
    }
    func onSelected(_ newValue: Bool) {
        guard selectedBackgroundView == nil else { return }
        if let numLabel = numLabel {
            numLabel.textColor = newValue ? #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1) : #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            numLabel.font = newValue ? UIFont(name: "Roboto-Light", size: 43) : UIFont(name: "Roboto-Light", size: 42)
        }
    }
}
