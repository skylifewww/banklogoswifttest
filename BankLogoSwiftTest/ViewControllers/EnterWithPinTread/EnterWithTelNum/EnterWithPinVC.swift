//
//  EnterWithPinVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 13.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CryptoSwift
import nuntius
import Locksmith

private let reuseIdentifierNumPadCell = "NumPadCell"

class EnterWithPinVC: UIViewController, UICollectionViewDelegateFlowLayout {
    

    @IBOutlet weak var enterTelNumLabel: UILabel!
    @IBOutlet weak var agreeTermsLabel: UILabel!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var telNumView: UIView!
    @IBOutlet weak var telNumLabel: UILabel!
    @IBOutlet weak var rightArrowButton: UIButton!
    @IBOutlet weak var roundedCheckBoxView: UIView!
    @IBOutlet weak var agreeTermsButton: UIButton!
    @IBOutlet weak var numberPudCollectionView: UICollectionView!
    @IBOutlet weak var greenAgreeView: UIView!
    @IBOutlet weak var numPadBottom: NSLayoutConstraint!
    @IBOutlet weak var numPadHeight: NSLayoutConstraint!
    @IBOutlet weak var numPadWidth: NSLayoutConstraint!
    @IBOutlet weak var needAgreeTermsLabel: UILabel!
    
    
    let arrNumPad = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "0", "backspace"]
    var number = ""
    var telNumberString = ""
    var isAgreeTerms = false
    var confirmationToken = ""
    let networkManager = NetworkManager()
    var isEnable = false
    let cryptoUtils = CryptoUtils()
    let encryptionService = IREncryptionService()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupViews()
 
    }

    //MARK:- IBAction Metods
    @IBAction func rightArrowButtonTapped(_ sender: UIButton) {
        if isEnable {
            goToEnterSMSCodeVC(confirmationToken: confirmationToken, numberTel: number)
        }
    }
    
    @IBAction func agreeTermsButtonTapped(_ sender: UIButton) {
        setupAgreeTermsButtonTapped()
    }
    
    @IBAction func termsButtonTapped(_ sender: UIButton) {
        if let url = URL(string: Constants.termsURL) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    private func goToEnterSMSCodeVC(confirmationToken: String, numberTel:String) {
        let enterSMSCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "EnterSMSCodeVC") as! EnterSMSCodeVC
        enterSMSCodeVC.confirmationToken = confirmationToken
        enterSMSCodeVC.numberTel = numberTel
        enterSMSCodeVC.numberTelStr = telNumberString
        self.presentDetail(enterSMSCodeVC)
    }
    
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")
            numPadBottom.constant = 0
            numPadHeight.constant = 240
            
        } else if (Device.IS_IPHONE_X){
            numPadBottom.constant = 16
        } else {
            numPadBottom.constant = -6
        }
        
        //MARK:- UICollectionViewFlowLayout
        let nibName = UINib(nibName: reuseIdentifierNumPadCell, bundle: nil)
        
        numberPudCollectionView.register(nibName, forCellWithReuseIdentifier: reuseIdentifierNumPadCell)
        
        let numItem = 3
        let widthArea = numberPudCollectionView.bounds.size.width - 2
        let heightArea = (UIScreen.main.bounds.size.height * 0.4) - 4
        let widthItem = floor(widthArea / CGFloat(numItem))
        let heightItem = heightArea / 4

        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: widthItem, height: heightItem)
        numberPudCollectionView.collectionViewLayout = layout
        
        //MARK:- Setup Views
        
        needAgreeTermsLabel.isHidden = true
        
        enterTelNumLabel.text = """
        Введи номер телефона
        чтобы войти или зарегистрироваться
        """
        setupRightArrowButtonWithNumber()
        telNumLabel.text = telNumberString
        rightArrowButton.setRoundButton(cornerRadius: 20)
        telNumView.setRoundBorderEdgeView(cornerRadius: 25, borderWidth: 1, borderColor: #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1))
        greenAgreeView.setRoundedView(cornerRadius: 5)
        roundedCheckBoxView.setRoundBorderEdgeView(cornerRadius: 12, borderWidth: 7, borderColor: #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1))
        greenAgreeView.backgroundColor = .clear
    }
    
    private func setupRightArrowButtonWithNumber() {
        
        if number.count == 13 && isAgreeTerms == true{

            needAgreeTermsLabel.isHidden = true
            networkManager.postRegisterRequest(cellPhone: number) { (confirmation, error) in
                if (error == nil && confirmation.count > 0){
                    self.confirmationToken = confirmation
                    print(confirmation)
                    self.isEnable = true
                    
                    self.rightArrowButton.setImage(UIImage(named: "rightArrowEnabled"), for:
                        UIControlState.normal)

                } else {
                    
                }
            }

        } else if number.count == 13 && isAgreeTerms == false{
            needAgreeTermsLabel.isHidden = false
            roundedCheckBoxView.setRoundBorderEdgeView(cornerRadius: 12, borderWidth: 7, borderColor: #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1))
            isEnable = false
            self.rightArrowButton.setImage(UIImage(named: "rightArrowDisabled"), for: UIControlState.normal)
        } else {
            isEnable = false
            self.rightArrowButton.setImage(UIImage(named: "rightArrowDisabled"), for: UIControlState.normal)
        }
    }
    
    private func setupAgreeTermsButtonTapped() {
        isAgreeTerms = !isAgreeTerms
        if isAgreeTerms {
            roundedCheckBoxView.setRoundBorderEdgeView(cornerRadius: 12, borderWidth: 1, borderColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
            greenAgreeView.backgroundColor = #colorLiteral(red: 0.5098039216, green: 0.7764705882, blue: 0.3176470588, alpha: 1)
            needAgreeTermsLabel.isHidden = true
        } else {
            roundedCheckBoxView.setRoundBorderEdgeView(cornerRadius: 12, borderWidth: 7, borderColor: #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1))
            greenAgreeView.backgroundColor = .clear
            needAgreeTermsLabel.isHidden = false
        }
        setupRightArrowButtonWithNumber()
    }
    
    private func prepareTelNumString() {
        if telNumberString.count == 3 {
            telNumberString.append("(")
            telNumLabel.text = telNumberString
        }
        if telNumberString.count == 7 {
            telNumberString.append(")")
            telNumLabel.text = telNumberString
        }
        if telNumberString.count == 11 ||  telNumberString.count == 14{
            telNumberString.append("-")
            telNumLabel.text = telNumberString
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

//MARK:- UICollectionViewDataSource
extension EnterWithPinVC:UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierNumPadCell, for: indexPath) as! NumPadCell
        
        cell.setupWithData(data: arrNumPad[indexPath.item])
        return cell
    }
}

//MARK:- UICollectionViewDelegate
extension EnterWithPinVC:UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = arrNumPad[indexPath.item]

        switch data {
        case "backspace":
            if telNumberString.count > 0 {
                telNumberString.removeLast()
                telNumLabel.text = telNumberString
            }
            number = telNumberString.filter { "+0123456789".contains($0) }
            setupRightArrowButtonWithNumber()

        case "+":
            
            if telNumberString == "" {
                telNumberString = "+"
                telNumLabel.text = telNumberString
            }

        case "0":
            
           prepareTelNumString()
            
            if telNumberString.count > 3  && telNumberString.count < 17{
                telNumberString.append("0")
                telNumLabel.text = telNumberString
            }
           prepareTelNumString()
            
           number = telNumberString.filter { "+0123456789".contains($0) }
           setupRightArrowButtonWithNumber()

        case "1", "2", "3", "4", "5", "6", "7", "8", "9":
            
            prepareTelNumString()


            if telNumberString.count > 0 && telNumberString.count < 17{
                telNumberString.append(data)
                telNumLabel.text = telNumberString
            }
            
           prepareTelNumString()

            number = telNumberString.filter { "+0123456789".contains($0) }
            setupRightArrowButtonWithNumber()

        default:
            print("default")
        }
    }
}
