//
//  NotClientVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 30.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class NotClientVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var goToTelegramButton: UIButton!
    @IBOutlet weak var descripLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    //MARK:- IBAction Metods
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismissDetail()
    }
    @IBAction func goToTelegramButtonTapped(_ sender: UIButton) {
        print("goToTelegramButtonTapped!!!!!!!!!!!!!!!!!!!!!")
    }
    
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")
            
        } else {
            
        }
        
        
        //MARK:- Setup Views
        
        descripLabel.text = """
        Воспользуйся всеми возможностями
        нового банка в Telegram.
        Управляй своими деньгами в чате,
        получай кэшбек и другие бонусы,
        совершай покупки в магазинах  в одно прикосновение
        """
        
        goToTelegramButton.setRoundButton(cornerRadius: 12)
        roundedView.setRoundedView(cornerRadius: 32.5)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
