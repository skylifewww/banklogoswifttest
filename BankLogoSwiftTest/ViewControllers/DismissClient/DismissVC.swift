//
//  DismissVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 03.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class DismissVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var untieButton: UIButton!
    @IBOutlet weak var descripLabel: UILabel!
    
    let networkManager = NetworkManager()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    
    // MARK: - IBAction metods
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismissDetail()
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func untieButtonTapped(_ sender: UIButton) {
    }
    
    private func prepareDeleteIdentitiesRequest()
    {

        networkManager.deleteIdentitiesRequest { (clientMessage, error) in
            if error == nil {
                print(clientMessage)
                
            } else {
                print(error!)
            }
        }

//
//                    //                    if ((UserDefaults.standard.value(forKey: "login")) != nil) {
//                    //                        UserDefaults.standard.set(login, forKey: "login")
//                    //                    }
//                    //
//                    //                    if ((UserDefaults.standard.value(forKey: "serverOpenKey")) != nil) {
//                    //                        UserDefaults.standard.set(serverOpenKey, forKey: "serverOpenKey")
//                    //                    }
//                    //
//                    //                    UserDefaults.standard.synchronize()
//
//                    //
//                    //                    static func clearUserData(){
//                    //                        UserDefaults.standard.removeObject(forKey: userSessionKey)
//                    //                    }
//
//                    //                    do {
//                    //                        try Locksmith.saveData(data: [Constants.keyLogin : login, Constants.keyTelNumber : telNumber, Constants.clientKeyPair: self.clientKeyPair!], forUserAccount: Constants.bankLogoAccount)
//                    //                    } catch {
//                    //                        print("Unable to save data")
//                    //                    }
//
//                    self.serverOpenKey = serverOpenKey
//
//                    //                    if let clientKeyPair = self.clientKeyPair{
//                    //                    self.createSharedSecret(clientKeyPair: self.clientKeyPair!, serverOpenKye: serverOpenKey)
//                    //                    }
//                } else {
//                    print(error!)
//                }
//            }
//        }
    }
    
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")
            
        } else {
            
        }
        
        
        //MARK:- Setup Views
        
                let string = """
                Ты потеряешь возможность
                безопасно подтверждать свои
                транзакции на данном устройстве.
                Лимиты платежей будут отключены
                до привязки нового устройства.
                """
        
        descripLabel.attributedText = string.prepareAttributedString(lineSpacing: 4.0, alignment: .center)
        
        cancelButton.setRoundButton(cornerRadius: 12)

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
