//
//  SettingsVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 01.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import DPLocalization

private let reuseIdentifierSettingsCell = "SettingsCell"

class SettingsVC: UIViewController {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var launguageView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    
    let arrTitles = ["logInApplication", "touch_id", "face_id", "PIN_code"]
    var isTouchIDEnabled = false
    var isFaceIDEnabled = false
    private var fillColor: UIColor = #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 1)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        updateLangSelector()
    }
    
    //MARK:- IBAction metods
    @IBAction func settingsButtonTapped(_ sender: UIButton) {
      dismissDetail()
    }
    @IBAction func disconnectButtonTapped(_ sender: UIButton) {
        goToDismissVC()
    }
    
    @IBAction func segmentedValueChanged(_ sender: UISegmentedControl) {

        switch (self.segmentedControl?.selectedSegmentIndex ?? 0) {
        case 0:
            
            dp_set_current_language(Language.RU.rawValue)
            UserDefaults.standard.set(Language.RU.rawValue, forKey: Constants.keyLanguage)
            UserDefaults.standard.synchronize()
        case 1:
            dp_set_current_language(Language.EN.rawValue)
            UserDefaults.standard.set(Language.EN.rawValue, forKey: Constants.keyLanguage)
            UserDefaults.standard.synchronize()
        default:
            dp_set_current_language(nil)
        }
        
        tableView.reloadData()
    }
    
   private func updateLangSelector() {
        if dp_get_current_language() == Language.EN.rawValue {
            self.segmentedControl?.selectedSegmentIndex = 1;
        }
        else if dp_get_current_language() == Language.RU.rawValue {
            self.segmentedControl?.selectedSegmentIndex = 0;
        }
            
        else if dp_get_current_language() == nil {
            self.segmentedControl?.selectedSegmentIndex = 0;
        }
    }
    
    
    private func goToDismissVC() {
        
        let dismissVC = self.storyboard?.instantiateViewController(withIdentifier: "DismissVC") as! DismissVC
        
        self.presentDetail(dismissVC)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        //        if (Device.IS_IPHONE_5){
        //
        //            print("Device.IS_IPHONE_5")
        //            numPadBottom.constant = 0
        //            numPadHeight.constant = 240
        //
        //        } else if (Device.IS_IPHONE_X){
        //            numPadBottom.constant = 16
        //        } else {
        //            numPadBottom.constant = -6
        //        }
        
        
        //MARK:- Setup Views
        
        let nibName = UINib(nibName: reuseIdentifierSettingsCell, bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: reuseIdentifierSettingsCell)
        launguageView.setRoundedShadowView(cornerRadius: 12, color: fillColor)
        shadowView.layoutIfNeeded()
        launguageView.layoutIfNeeded()
        shadowView.setRoundedShadowView(cornerRadius: 12, color: fillColor)
        tableView.setRoundedView(cornerRadius: 12)
        
    }

@objc func turnOrOffCell(sender:UISwitch){
    
    switch sender.tag {
    case 1:
        print("switchControl.tag \(sender.tag)")
        if sender.isOn{
            isTouchIDEnabled = true
            print("isTouchIDEnabled \(isTouchIDEnabled)")

        } else {
            isTouchIDEnabled = false
            print("isTouchIDEnabled \(isTouchIDEnabled)")
            
        }
    case 2:
        print("switchControl.tag \(sender.tag)")
        if sender.isOn{
            isFaceIDEnabled = true
            print("isFaceIDEnabled \(isFaceIDEnabled)")

        } else {
            isFaceIDEnabled = true
            print("isFaceIDEnabled \(isFaceIDEnabled)")

        }
    default:
        print("default")
    }
  }
    
    @objc private func goToEnterPIN1VC() {
        let enterPin1VC = self.storyboard?.instantiateViewController(withIdentifier: "EnterPin1VC") as! EnterPin1VC
        enterPin1VC.changePIN = true
        self.presentDetail(enterPin1VC)
    }
}


//MARK:- UITableViewDataSource
extension SettingsVC:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierSettingsCell, for: indexPath) as! SettingsCell

        cell.setupWithData(data: arrTitles[indexPath.row], row:indexPath.row)
        cell.switchCell.addTarget(self, action: #selector(turnOrOffCell), for: .valueChanged)
        cell.changeButton.addTarget(self, action: #selector(goToEnterPIN1VC), for: .touchUpInside)
        return cell
    }
}

//
////MARK:- UITableViewDelegate
//extension SettingsVC:UITableViewDelegate
//{

//}
