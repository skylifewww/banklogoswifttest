//
//  SettingsCell.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 02.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import DPLocalization

class SettingsCell: UITableViewCell {

    @IBOutlet weak var switchCell: UISwitch!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var changeButton: UIButton!
    
    var isDrawTopLine = false
    var isDrawBottomLine = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        switchCell.layer.cornerRadius = switchCell.frame.height / 2
        
    }
    
    func setupWithData(data: String, row:Int){
        switchCell.isHidden = false
        titleLabel.text = DPLocalizedString(data, nil)
        changeButton.isHidden = true
        changeButton.isUserInteractionEnabled = false
        isDrawTopLine = true
        isDrawBottomLine = false
        self.isUserInteractionEnabled = true
        
        if row == 0 {
            isDrawTopLine = false
            switchCell.isHidden = true
        }
        
        if row == 3 {
            switchCell.isHidden = true
            isDrawBottomLine = true
            changeButton.isHidden = false
            changeButton.isUserInteractionEnabled = true
        }
        switchCell.tag = row
        
        if data == "logInApplication" {
            switchCell.isHidden = true
            titleLabel.font = UIFont(name: "Roboto-Medium", size: 20.0)
        }
        
        if data == "PIN_code" {
            switchCell.isHidden = true
        }
    }

    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        if isDrawTopLine {
            contentView.drawLine(start: CGPoint(x:contentView.bounds.minX + 16, y: contentView.bounds.minY), end: CGPoint(x: contentView.bounds.maxX - 16, y: contentView.bounds.minY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        }
        if isDrawBottomLine {
            contentView.drawLine(start: CGPoint(x:contentView.bounds.minX + 16, y: contentView.bounds.maxY), end: CGPoint(x: contentView.bounds.maxX - 16, y: contentView.bounds.maxY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        changeButton.isHidden = true
        changeButton.isUserInteractionEnabled = false
        switchCell.isHidden = false
        titleLabel.text = ""
//        changeLabel.isHidden = true
//        backChevron.isHidden = true
        isDrawTopLine = false
        isDrawBottomLine = false
    }
}
