//
//  FunctionsVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 30.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

private let reuseIdentifierFunctionCell = "FunctionCell"

class FunctionsVC: UIViewController {

    @IBOutlet weak var arrowLeftButton: UIButton!
    @IBOutlet weak var arrowRightButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var dotaRight: UIView!
    @IBOutlet weak var dotaLeft: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var shadowView: UIView!
    
    
    
    private var fillColor: UIColor = #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    //MARK:- IBAction Metods
    
    @IBAction func settingsButtonTapped(_ sender: UIButton) {
        
//        let popUpErrorVC = self.storyboard?.instantiateViewController(withIdentifier: "PopUpErrorVC") as! PopUpErrorVC
//
//        self.present(popUpErrorVC, animated: false) {
//            print("popUpErrorVC")
//        }
        goToConfirmTransferVC()
    }
    
    @IBAction func arrowRightButtonTapped(_ sender: UIButton) {
        
        let indexPath = IndexPath(row: 1, section: 0)
        collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func arrowLeftButtonTapped(_ sender: UIButton) {
        let indexPath = IndexPath(row: 0, section: 0)
        collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    private func goToConfirmTransferVC() {
        
        let confirmTransferVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmTransferVC") as! ConfirmTransferVC
        
        self.presentDetail(confirmTransferVC)
    }
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
//        if (Device.IS_IPHONE_5){
//
//            print("Device.IS_IPHONE_5")
//            numPadBottom.constant = 0
//            numPadHeight.constant = 240
//
//        } else if (Device.IS_IPHONE_X){
//            numPadBottom.constant = 16
//        } else {
//            numPadBottom.constant = -6
//        }
        
        //MARK:- UICollectionViewFlowLayout
//        let nibName = UINib(nibName: reuseIdentifierNumPadCell, bundle: nil)
//
//        numberPudCollectionView.register(nibName, forCellWithReuseIdentifier: reuseIdentifierNumPadCell)
//
//        let numItem = 3
//        let widthArea = numberPudCollectionView.bounds.size.width - 2
//        let heightArea = (UIScreen.main.bounds.size.height * 0.4) - 4
//        let widthItem = floor(widthArea / CGFloat(numItem))
//        let heightItem = heightArea / 4
//
//        let layout = UICollectionViewFlowLayout()
//        layout.minimumLineSpacing = 1
//        layout.minimumInteritemSpacing = 1
//        layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
//        layout.itemSize = CGSize(width: widthItem, height: heightItem)
//        numberPudCollectionView.collectionViewLayout = layout
        
        //MARK:- Setup Views
 
        self.arrowLeftButton.alpha = 0.0
        self.arrowRightButton.alpha = 1.0
        dotaRight.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
        dotaLeft.backgroundColor = #colorLiteral(red: 0.7058823529, green: 0.7058823529, blue: 0.7058823529, alpha: 1)
        dotaLeft.setRoundedView(cornerRadius: 5.5)
        dotaRight.setRoundedView(cornerRadius: 5.5)
        shadowView.setRoundedShadowView(cornerRadius: 12, color: fillColor)
  
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK:- UICollectionViewDataSource
extension FunctionsVC:UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierFunctionCell, for: indexPath) as! FunctionCell
        cell.containerView.setRoundedView(cornerRadius: 12)

        cell.setupWithItem(item: indexPath.item)
        return cell
    }
}

//MARK:- UICollectionViewDelegate
extension FunctionsVC:UICollectionViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let contX = scrollView.contentOffset.x
        
        let width = (collectionView.contentSize.width - collectionView.bounds.size.width);

        
        if contX == width{
            
            dotaLeft.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            dotaRight.backgroundColor = #colorLiteral(red: 0.7058823529, green: 0.7058823529, blue: 0.7058823529, alpha: 1)
            UIView.animate(withDuration: 0.6, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.arrowLeftButton.alpha = 1.0
                self.arrowRightButton.alpha = 0.0
            }) { (end) in
                
            }

        } else if contX == 0{
            
            UIView.animate(withDuration: 0.6, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.arrowLeftButton.alpha = 0.0
                self.arrowRightButton.alpha = 1.0
            }) { (end) in
                
            }
            dotaRight.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            dotaLeft.backgroundColor = #colorLiteral(red: 0.7058823529, green: 0.7058823529, blue: 0.7058823529, alpha: 1)
        }
    }
}
