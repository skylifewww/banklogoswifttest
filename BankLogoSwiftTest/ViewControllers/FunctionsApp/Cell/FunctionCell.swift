//
//  FunctionCell.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 30.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class FunctionCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var descrBottomLabel: UILabel!

    private var fillColor: UIColor = #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 1)
    
    
    func setupWithItem(item: Int){
        if item == 0 {

            containerView.setRoundedShadowView(cornerRadius: 12, color: fillColor)
            googleButton.setRoundButton(cornerRadius: 12)

            googleButton.isHidden = false
            titleLabel.text = "Google Pay"
            containerView.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            imageView.image = #imageLiteral(resourceName: "Illustration_1")
            
            let string = """
            Ты сможешь совершать
            покупки в магазинах,
            оплачивать рестораны,
            отели и многое другое в одно
            прикосновение телефона.
            """
            
            descriptLabel.attributedText = prepareAttributedString(string: string)

        } else if item == 1 {
            

            containerView.setRoundedShadowView(cornerRadius: 12, color: fillColor)
            googleButton.isHidden = true
            containerView.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.2235294118, blue: 0.7568627451, alpha: 1)
            titleLabel.text = "Лимиты"
            imageView.image = #imageLiteral(resourceName: "Illustration_2")
            let string = """
            Установи лимит суммы
            платежа. Все операции выше
            установленной суммы будут
            требовать подтверждения
            в приложении.
            """
            descriptLabel.attributedText = prepareAttributedString(string: string)
        }
    }
    
    private func prepareAttributedString(string: String) -> NSMutableAttributedString{
        let attributedString = NSMutableAttributedString(string: string)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        return attributedString
    }
}
