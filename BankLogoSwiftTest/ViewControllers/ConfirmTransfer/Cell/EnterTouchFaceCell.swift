//
//  EnterTouchFaceCell.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 10.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class EnterTouchFaceCell: UICollectionViewCell {
    
    @IBOutlet weak var enterByLabel: UILabel!
    @IBOutlet weak var touchAndFaceImageView: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupWithItem(biometric: BiometricSupport){
        if biometric == .TouchID {
            
            enterByLabel.text = "Вход по отпечатку пальца"
            touchAndFaceImageView.image = #imageLiteral(resourceName: "touch_small")
            
        } else if biometric == .FaceID  {
            
            enterByLabel.text = "Вход, распознавание лица"
            touchAndFaceImageView.image = #imageLiteral(resourceName: "face")
        }
    }

}
