//
//  CardCell.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 31.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class CardCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var transferByNumberLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var sumTransfer: UILabel!
    @IBOutlet weak var confirmTimeLabel: UILabel!
    
}
