//
//  GuideView.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 04.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class GuideView: UIView {
    
    @IBOutlet var contentView: GuideView!
    @IBOutlet weak var overTransferView: UIView!
    @IBOutlet weak var overDenyButton: UIView!
    @IBOutlet weak var overOkButton: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    @IBOutlet weak var okLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("GuideView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        cancelLabel.translatesAutoresizingMaskIntoConstraints = false
        cancelLabel.centerXAnchor.constraint(equalTo: overDenyButton.centerXAnchor).isActive = true
        cancelLabel.topAnchor.constraint(equalTo: overDenyButton.bottomAnchor, constant:5).isActive = true
        okLabel.translatesAutoresizingMaskIntoConstraints = false
        okLabel.centerXAnchor.constraint(equalTo: overOkButton.centerXAnchor).isActive = true
        okLabel.topAnchor.constraint(equalTo: overOkButton.bottomAnchor, constant:5).isActive = true
    }
}
