//
//  TransferView.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 31.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class TransferView: UIView {
    
    @IBOutlet var contentView: TransferView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var transferByNumber: UILabel!
    @IBOutlet weak var transferValueLabel: UILabel!
    @IBOutlet weak var confirmTimeLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("TransferView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        userPhotoImageView.setRoundedImageView(cornerRadius: 26)
    }
    
    public func drawDottedLineWithLabel(){
        
        transferValueLabel.drawDottedLine(start: CGPoint(x: transferValueLabel.bounds.minX, y: transferValueLabel.bounds.minY), end: CGPoint(x: transferValueLabel.bounds.maxX, y: transferValueLabel.bounds.minY), strokeColor: #colorLiteral(red: 0.7058823529, green: 0.7058823529, blue: 0.7058823529, alpha: 1), lineWidth: 0.6, lineDashPattern: [7, 5])
        transferValueLabel.drawDottedLine(start: CGPoint(x: transferValueLabel.bounds.minX, y: transferValueLabel.bounds.maxY), end: CGPoint(x: transferValueLabel.bounds.maxX, y: transferValueLabel.bounds.maxY), strokeColor: #colorLiteral(red: 0.7058823529, green: 0.7058823529, blue: 0.7058823529, alpha: 1), lineWidth: 0.6, lineDashPattern: [7, 5])
        
    }
    
    
    
    public func setTextForTransferByNumberLabel(numberTelStr:String){
        
        let numStr = String(numberTelStr.prefix(5)) + "*)***-**" + String(numberTelStr.suffix(2))
        print(numStr)
        
        let string = "Перевод по номеру\n\(numStr)"
        transferByNumber.attributedText = string.prepareAttributedString(lineSpacing: 2.0, alignment: .center)
        
    }

}
