//
//  EnterTouchAndFaceView.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 04.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

private let heightNumPadView:CGFloat = 580
private let heightTouchView:CGFloat = 280

enum State {
    case closed
    case open
    
    var opposite: State {
        return self == .open ? .closed : .open
    }
}

class EnterTouchAndFaceView: UIView,  EnterTouchViewDelegate, EnterPINViewDelegate{

    @IBOutlet weak var blurView: UIView!
    @IBOutlet var contentView: EnterTouchAndFaceView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tildaView: UIView!
    @IBOutlet weak var enterTitleLabel: UILabel!
    @IBOutlet weak var enterTouchView: EnterTouchView!
    @IBOutlet weak var enterPINView: EnterPINView!
    
    var state: State = .closed
    var runningAnimators: [UIViewPropertyAnimator] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        enterTouchView.layoutIfNeeded()
        enterPINView.layoutIfNeeded()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EnterTouchAndFaceView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        enterTouchView.delegate = self
        enterPINView.delegate = self

        containerView.roundTop(radius: 29)
        tildaView.setRoundedView(cornerRadius: 6)
        
        enterTouchView.alpha = 1
        self.bringSubview(toFront: enterTouchView)
        enterPINView.alpha = 0
        
        setupViews()
    }
    
    func animateView(to state: State, duration: TimeInterval) {
        
        guard runningAnimators.isEmpty else { return }
        
        let basicAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeIn, animations: nil)
        
        basicAnimator.addAnimations {
            switch state {
            case .open:
                self.enterPINView.alpha = 1
                self.bringSubview(toFront: self.enterPINView)
                self.enterTouchView.alpha = 0
                self.containerView.frame = CGRect(x: 0, y: self.bounds.height - heightNumPadView, width: self.bounds.width, height: heightNumPadView)
                
            case .closed:
                self.enterTouchView.alpha = 1
                self.bringSubview(toFront: self.enterTouchView)
                self.enterPINView.alpha = 0
                self.containerView.frame = CGRect(x: 0, y: self.bounds.height - heightTouchView, width: self.bounds.width, height: heightTouchView)
            }
            self.layoutIfNeeded()
        }
        basicAnimator.addCompletion { (animator) in
            self.runningAnimators.removeAll()
            self.state = self.state.opposite
        }
        runningAnimators.append(basicAnimator)
    }
    
    func setupViews() {

        self.layoutIfNeeded()
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onDrag(_:)))
        self.containerView.addGestureRecognizer(panGesture)
    }
    
    @objc func onDrag(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            animateView(to: state.opposite, duration: 0.3)
        case .changed:
            let translation = gesture.translation(in: containerView)
            let fraction = abs(translation.y / heightNumPadView)
            runningAnimators.forEach { (animator) in
                animator.fractionComplete = fraction
            }
        case .ended:
            runningAnimators.forEach{ $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
        default:
            break
        }
    }
    
    func prepareEnterPIN(_ enterTouchView: EnterTouchView) {
        
        self.enterPINView.alpha = 1
        self.bringSubview(toFront: self.enterPINView)
        enterTouchView.alpha = 0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.containerView.frame = CGRect(x: 0, y: self.bounds.height - heightNumPadView, width: self.bounds.width, height: heightNumPadView)
        }) { (_) in
            self.state = self.state.opposite
        }
    }
    
    func prepareEnterTouch(_ enterPINView: EnterPINView) {
        self.enterTouchView.alpha = 1
        self.bringSubview(toFront: self.enterTouchView)
        enterPINView.alpha = 0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.containerView.frame = CGRect(x: 0, y: self.bounds.height - heightTouchView, width: self.bounds.width, height: heightTouchView)
        }) { (_) in
            self.state = self.state.opposite
        }
    }
    
    func enterPINComplite(_ enterPINView: EnterPINView) {
        dismissAnimated()
    }
    
    func enterBiometricComplite(_ enterTouchView: EnterTouchView) {
        dismissAnimated()
    }
    
    private func dismissAnimated(){
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
}
