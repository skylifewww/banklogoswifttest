//
//  EnterPINView.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 09.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import nuntius
import CryptoSwift
import Alamofire
import SwiftyJSON
import Locksmith

private let reuseIdentifierNumPadCell = "NumPadCell"


@objc protocol EnterPINViewDelegate: class {

  @objc optional  func prepareEnterTouch(_ enterPINView: EnterPINView)
  @objc optional  func enterPINComplite(_ enterPINView: EnterPINView)
    
}

class EnterPINView: UIView, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var containerStackView: UIView!
    @IBOutlet var contentView: EnterPINView!
    @IBOutlet weak var enterPINLabel: UILabel!
    @IBOutlet weak var enableAccessButton: UIButton!
    @IBOutlet weak var numPadCollectionView: UICollectionView!
    @IBOutlet weak var dota1: UIView!
    @IBOutlet weak var dota2: UIView!
    @IBOutlet weak var dota3: UIView!
    @IBOutlet weak var dota4: UIView!
    
    weak var delegate: EnterPINViewDelegate?
    
    let cryptoUtils = CryptoUtils()
    let networkManager = NetworkManager()
    
    let arrNumPad = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "touchNumPad", "0", "backspace"]
    var number = ""
    var isSuccessPIN = false
    var isFirstStep = true
    
    var isAuth:Bool? {
        didSet {
            print("isAuth did set \(isAuth!)")
        }
    }
    
    var confirmationToken:String = ""
    var numberTel:String? {
        didSet {
            print("numberTel did set " + numberTel!)
        }
    }
    
    var login:String? {
        didSet {
            print("login did set " + login!)
        }
    }
    
    var pin:String? {
        didSet {
            print("pin did set " + pin!)
        }
    }
    
    var serverOpenKey:String? {
        didSet {
            print("serverOpenKey did set " + serverOpenKey!)
        }
    }
    let axlsign = AxlSign()
    
    var clientKeyPair:AxlSign.Keys?
    var clientProof:String? {
        didSet {
            print("clientProof did set " + clientProof!)
        }
    }
    var clientNonce:String? {
        didSet {
            print("clientNonce did set " + clientNonce!)
        }
    }
    var serverNonce:String? {
        didSet {
            print("serverNonce did set " + serverNonce!)
        }
    }
    var getSecretKey:String? {
        didSet {
            print("getSecretKey did set " + getSecretKey!)
        }
    }
    var setSecretKey:[UInt8]? {
        didSet {
            print("getSecretKey did set " + (setSecretKey?.toBase64())!)
        }
    }
    
    var salt:String? {
        didSet {
            print("salt did set " + salt!)
        }
    }
    
    var sharedSecret:[UInt8]?{
        didSet {
            print("sharedSecret did set ")
        }
    }
    
    var sharedSecretSha1:[UInt8]?{
        didSet {
            print("sharedSecretSha1 did set ")
        }
    }
    
    var iterations = 4096
    var saltedPassword:[UInt8]?
    var authMessage:String? {
        didSet {
            print("authMessage did set " + authMessage!)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    @IBAction func enableAccessButtonTapped(_ sender: UIButton) {
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        containerStackView.drawLine(start: CGPoint(x:containerStackView.bounds.minX, y: containerStackView.bounds.minY), end: CGPoint(x: containerStackView.bounds.maxX, y: containerStackView.bounds.minY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        containerStackView.drawLine(start: CGPoint(x:containerStackView.bounds.minX, y: containerStackView.bounds.maxY), end: CGPoint(x: containerStackView.bounds.maxX, y: containerStackView.bounds.maxY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)

    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EnterPINView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setupViews()
    }
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
        if (Device.IS_IPHONE_5){
            
            print("Device.IS_IPHONE_5")
//            numPadBottom.constant = 0
//            numPadHeight.constant = 240
            
        } else if (Device.IS_IPHONE_X){
//            numPadBottom.constant = 16
        } else {
//            numPadBottom.constant = -6
        }
        
        //MARK:- UICollectionViewFlowLayout
        let nibName = UINib(nibName: reuseIdentifierNumPadCell, bundle: nil)
        
        numPadCollectionView.register(nibName, forCellWithReuseIdentifier: reuseIdentifierNumPadCell)
        
        let numItem = 3
        let widthArea = numPadCollectionView.bounds.size.width - 2
        let heightArea = (UIScreen.main.bounds.size.height * 0.4) - 4
        let widthItem = floor(widthArea / CGFloat(numItem))
        let heightItem = heightArea / 4
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: widthItem, height: heightItem)
        numPadCollectionView.collectionViewLayout = layout
        
        //MARK:- Setup Views

        enterPINLabel.text =
        """
        Введи PIN-код
        
        """
        
        dota1.setRoundedView(cornerRadius: 5)
        dota2.setRoundedView(cornerRadius: 5)
        dota3.setRoundedView(cornerRadius: 5)
        dota4.setRoundedView(cornerRadius: 5)
        
    }
    
    private func prepareUISMSCodeError(errorPIN:ErrorPIN){
        
        enterPINLabel.text = errorPIN.rawValue
        
        enterPINLabel.textColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dota1.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dota2.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dota3.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        dota4.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)

        number = ""
    }
    
    private func prepareUIFirstStep(){
        
        enterPINLabel.textColor = #colorLiteral(red: 0.2863244414, green: 0.3505810201, blue: 0.412160933, alpha: 1)
        dota1.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
        dota2.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
        dota3.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
        dota4.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
    }
    
    //MARK:- Network Metods
    
    private func prepareAuthGetScramRequest(pin: String){

        
        guard let keychainDict = Locksmith.loadDataForUserAccount(userAccount: Constants.bankLogoAccount) else {
            print("error")
            return}
        
        guard let login = keychainDict[Constants.keyLogin] as? String else {
            print("error")
            return}
        guard let storedKey = keychainDict[Constants.storedKey] as? [UInt8] else {
            print("error")
            return}
  
            let pinSha1 = pin.bytes.sha1()
            sharedSecretSha1 = cryptoUtils.setSecret(leftArray: storedKey, rightArray: pinSha1)
            self.login = login
            
            self.networkManager.getScrumDeviceRequest(confirmationToken: "", login: login) { (serverNonce, salt, iterations, error) in
                if error == nil {
                    self.serverNonce = serverNonce.replacingOccurrences(of: "\\/", with: "/")
                    print(serverNonce)
                    
                    self.salt = salt.replacingOccurrences(of: "\\/", with: "/")
                    print(salt)
                    
                    self.iterations = iterations
                    print(iterations)
                    
                    self.generateClientNonce()
                    
                } else {
                    print(error!)
                }
            }
    }
    
    private func generateClientNonce(){
        
        var keyData = Data(count: 64)
        let result = keyData.withUnsafeMutableBytes {
            SecRandomCopyBytes(kSecRandomDefault, 64, $0)
        }
        if result == errSecSuccess {
            let str = keyData.base64EncodedString()
            
            if str.contains("/"){
                generateClientNonce()
            } else {
                self.clientNonce = str
                createPassword()
            }
            
        } else {
            //            print("Problem generating random bytes")
            //            return nil
        }
    }
    
    private func createPassword(){
        let saltedPassword = cryptoUtils.prepareSaltedPassword(salt: salt!, iterations: 4096, pinHash: (pin?.bytes)!, secretKey: sharedSecretSha1!)
        
        let authMessage = cryptoUtils.prepareAuthMessage(login: login!, clientNonce: clientNonce!, serverNonce: serverNonce!, salt: salt!, iterations: 4096)
        print(authMessage)
        
        let clientProof = cryptoUtils.prepareProof(authMessage: authMessage, saltedPassword: saltedPassword)
        
        self.preparePostScrumLoginRequest(clientNonce: clientNonce!, clientProof: clientProof, login: login!, serverNonce: serverNonce!)
    }
    
    private func preparePostScrumLoginRequest(clientNonce: String, clientProof: String, login: String, serverNonce: String){
        
        self.networkManager.postScrumLoginRequest(clientNonce: clientNonce, clientProof: clientProof, login: login, serverNonce: serverNonce, confirmationToken: confirmationToken, completionHandler: { (state, verifier, error) in
            if error == nil && state == "Complete" {
                self.delegate?.enterPINComplite!(self)
//                self.goToTouchAndFaceVC()
                print(state)
                print(verifier)
            } else {
                print(error!)
            }
        })
    }
    
}

//MARK:- UICollectionViewDataSource
extension EnterPINView:UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierNumPadCell, for: indexPath) as! NumPadCell
        
        cell.setupWithData(data: arrNumPad[indexPath.item])
        return cell
    }
}

//MARK:- UICollectionViewDelegate
extension EnterPINView:UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = arrNumPad[indexPath.item]
        
        switch data {
        case "backspace":
            
            if number.count == 1{
                number = ""
                dota1.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
                
            } else if number.count == 2{
                number.removeLast()
                dota2.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
                
            } else if number.count == 3{
                number.removeLast()
                dota3.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
                
            } else if number.count == 4{
                number.removeLast()
                dota4.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.8862745098, blue: 0.9137254902, alpha: 1)
            }
            print(number)
            
            
        case "touchNumPad":
            
            self.delegate?.prepareEnterTouch!(self)
            
        case "1", "2", "3", "4", "5", "6", "7", "8", "9", "0":
            
            if number.count == 0{
                
//                if isFirstStep {
//                    enterPINLabel.text =
//                    """
//                    Придумай PIN-код
//
//                    """
//                }
                
//                prepareUIFirstStep()
                number.append(data)
                dota1.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                
            } else if number.count == 1{
                number.append(data)
                dota2.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                
            } else if number.count == 2{
                number.append(data)
                dota3.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
            } else if number.count == 3{
                
                dota4.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
                print(number)
                number.append(data)
                self.pin = number

                prepareAuthGetScramRequest(pin: number)

            }
            
        default:
            print("default")
        }
    }
}


