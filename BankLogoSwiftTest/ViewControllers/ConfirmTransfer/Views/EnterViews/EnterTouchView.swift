//
//  EnterTouchView.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 09.08.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import LocalAuthentication
import nuntius
import CryptoSwift
import Alamofire
import SwiftyJSON
import Locksmith


private let reuseIdentifierEnterTouchFaceCell = "EnterTouchFaceCell"

@objc protocol EnterTouchViewDelegate: class {
   @objc optional func prepareEnterPIN(_ enterTouchView: EnterTouchView)
   @objc optional func enterBiometricComplite(_ enterTouchView: EnterTouchView)
}

class EnterTouchView: UIView, UICollectionViewDelegateFlowLayout{

    @IBOutlet var contentView: EnterTouchView!
    @IBOutlet weak var containerButtonView: UIView!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var enterBylabel: UILabel!
    @IBOutlet weak var touchFaceButton: UIButton!
//    @IBOutlet weak var rightButton: UIButton!
//    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: EnterTouchViewDelegate?
    var currentBiometric:BiometricSupport?
    let cryptoUtils = CryptoUtils()
    let networkManager = NetworkManager()
    let pin = "1234"
    
    var confirmationToken:String = ""
    var numberTel:String? {
        didSet {
            print("numberTel did set " + numberTel!)
        }
    }
    
    var login:String? {
        didSet {
            print("login did set " + login!)
        }
    }
    
    var serverOpenKey:String? {
        didSet {
            print("serverOpenKey did set " + serverOpenKey!)
        }
    }
    let axlsign = AxlSign()
    
    var clientKeyPair:AxlSign.Keys?
    var clientProof:String? {
        didSet {
            print("clientProof did set " + clientProof!)
        }
    }
    var clientNonce:String? {
        didSet {
            print("clientNonce did set " + clientNonce!)
        }
    }
    var serverNonce:String? {
        didSet {
            print("serverNonce did set " + serverNonce!)
        }
    }
    var getSecretKey:String? {
        didSet {
            print("getSecretKey did set " + getSecretKey!)
        }
    }
    var setSecretKey:[UInt8]? {
        didSet {
            print("getSecretKey did set " + (setSecretKey?.toBase64())!)
        }
    }
    
    var salt:String? {
        didSet {
            print("salt did set " + salt!)
        }
    }
    
    var sharedSecret:[UInt8]?{
        didSet {
            print("sharedSecret did set ")
        }
    }
    
    var sharedSecretSha1:[UInt8]?{
        didSet {
            print("sharedSecretSha1 did set ")
        }
    }
    
    var iterations = 4096
    var saltedPassword:[UInt8]?
    var authMessage:String? {
        didSet {
            print("authMessage did set " + authMessage!)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    @IBAction func enterButtonTapped(_ sender: UIButton) {
        self.delegate?.prepareEnterPIN!(self)
    }
    
    @IBAction func touchFaceButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func rightButtonTapped(_ sender: UIButton) {
        let indexPath = IndexPath(row: 1, section: 0)
        collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func leftButtonTapped(_ sender: UIButton) {
        let indexPath = IndexPath(row: 0, section: 0)
        collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    //MARK:- Biometric Metods
    private func authenticateUser() {
        
        let context = LAContext()
        
        var error: NSError?
        
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &error) {
            
            // Device can use biometric authentication
            context.evaluatePolicy(
                LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Access requires authentication",
                reply: {(success, error) in
                    DispatchQueue.main.async {
                        
                        if let err = error {
                            
                            switch err._code {
                                
                            case LAError.Code.systemCancel.rawValue:
                                self.notifyUser("Session cancelled",
                                                err: err.localizedDescription)
                                
                            case LAError.Code.userCancel.rawValue:
                                self.notifyUser("Please try again",
                                                err: err.localizedDescription)
                                
                            case LAError.Code.userFallback.rawValue:
                                self.notifyUser("Authentication",
                                                err: "Password option selected")
                                // Custom code to obtain password here
                                
                            default:
                                self.notifyUser("Authentication failed",
                                                err: err.localizedDescription)
                            }
                            
                        } else {
                            
                            self.prepareAuthGetScramRequest(pin: self.pin)
                            
                            self.notifyUser("Authentication Successful",
                                            err: "You now have full access")
                        }
                    }
            })
            
        } else {
            // Device cannot use biometric authentication
            if let err = error {
                switch err.code {
                    
                case LAError.Code.biometryNotEnrolled.rawValue:
                    notifyUser("User is not enrolled",
                               err: err.localizedDescription)
                    
                case LAError.Code.passcodeNotSet.rawValue:
                    notifyUser("A passcode has not been set",
                               err: err.localizedDescription)
                    
                    
                case LAError.Code.biometryNotAvailable.rawValue:
                    notifyUser("Biometric authentication not available",
                               err: err.localizedDescription)
                default:
                    notifyUser("Unknown error",
                               err: err.localizedDescription)
                }
            }
        }
    }
    
    func notifyUser(_ msg: String, err: String?) {
        let alert = UIAlertController(title: msg,
                                      message: err,
                                      preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        
//        self.present(alert, animated: true,
//                     completion: nil)
    }
    
    private func checkTouchOrFaceIDAvaible(){
        let context = LAContext()
        
        var error: NSError?
        
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &error) {
            
            if (context.biometryType == LABiometryType.faceID) {
                currentBiometric = .FaceID
                print("FaceID!!!!!!!!!!!!!!!!!!!!!!!!")
            } else if context.biometryType == LABiometryType.touchID {
                currentBiometric = .TouchID
                print("TouchID!!!!!!!!!!!!!!!!!!!!!!")
            } else {
                currentBiometric = .NoBiometric
                print("NoBiometric!!!!!!!!!!!!!!!!!!!")
            }
        }
    }
    
    //MARK:- required init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        containerButtonView.drawLine(start: CGPoint(x:containerButtonView.bounds.minX, y: containerButtonView.bounds.minY), end: CGPoint(x: containerButtonView.bounds.maxX, y: containerButtonView.bounds.minY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        containerButtonView.drawLine(start: CGPoint(x:containerButtonView.bounds.minX, y: containerButtonView.bounds.maxY), end: CGPoint(x: containerButtonView.bounds.maxX, y: containerButtonView.bounds.maxY), strokeColor: #colorLiteral(red: 0.8765067458, green: 0.876527369, blue: 0.8765162826, alpha: 1), lineWidth: 1)
        enterButton.setRoundedView(cornerRadius: 12)
        
        let widthItem = collectionView.bounds.size.width
        let heightItem = collectionView.bounds.size.height
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: widthItem, height: heightItem)
        collectionView.collectionViewLayout = layout
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EnterTouchView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        checkTouchOrFaceIDAvaible()
        
        let nibName = UINib(nibName: reuseIdentifierEnterTouchFaceCell, bundle: nil)
        collectionView.register(nibName, forCellWithReuseIdentifier: reuseIdentifierEnterTouchFaceCell)
        
//        self.leftButton.alpha = 0.0
//        self.rightButton.alpha = 1.0
        
    }
    
    //MARK:- Network Metods
    
    private func prepareAuthGetScramRequest(pin: String){
        
        
        guard let keychainDict = Locksmith.loadDataForUserAccount(userAccount: Constants.bankLogoAccount) else {
            print("error")
            return}
        
        guard let login = keychainDict[Constants.keyLogin] as? String else {
            print("error")
            return}
        guard let storedKey = keychainDict[Constants.storedKey] as? [UInt8] else {
            print("error")
            return}
        
        let pinSha1 = pin.bytes.sha1()
        sharedSecretSha1 = cryptoUtils.setSecret(leftArray: storedKey, rightArray: pinSha1)
        self.login = login
        
        self.networkManager.getScrumDeviceRequest(confirmationToken: "", login: login) { (serverNonce, salt, iterations, error) in
            if error == nil {
                self.serverNonce = serverNonce.replacingOccurrences(of: "\\/", with: "/")
                print(serverNonce)
                
                self.salt = salt.replacingOccurrences(of: "\\/", with: "/")
                print(salt)
                
                self.iterations = iterations
                print(iterations)
                
                self.generateClientNonce()
                
            } else {
                print(error!)
            }
        }
    }
    
    private func generateClientNonce(){
        
        var keyData = Data(count: 64)
        let result = keyData.withUnsafeMutableBytes {
            SecRandomCopyBytes(kSecRandomDefault, 64, $0)
        }
        if result == errSecSuccess {
            let str = keyData.base64EncodedString()
            
            if str.contains("/"){
                generateClientNonce()
            } else {
                self.clientNonce = str
                createPassword()
            }
            
        } else {
            //            print("Problem generating random bytes")
            //            return nil
        }
    }
    
    private func createPassword(){
        let saltedPassword = cryptoUtils.prepareSaltedPassword(salt: salt!, iterations: 4096, pinHash: (pin.bytes), secretKey: sharedSecretSha1!)
        
        let authMessage = cryptoUtils.prepareAuthMessage(login: login!, clientNonce: clientNonce!, serverNonce: serverNonce!, salt: salt!, iterations: 4096)
        print(authMessage)
        
        let clientProof = cryptoUtils.prepareProof(authMessage: authMessage, saltedPassword: saltedPassword)
        
        self.preparePostScrumLoginRequest(clientNonce: clientNonce!, clientProof: clientProof, login: login!, serverNonce: serverNonce!)
    }
    
    private func preparePostScrumLoginRequest(clientNonce: String, clientProof: String, login: String, serverNonce: String){
        
        self.networkManager.postScrumLoginRequest(clientNonce: clientNonce, clientProof: clientProof, login: login, serverNonce: serverNonce, confirmationToken: confirmationToken, completionHandler: { (state, verifier, error) in
            if error == nil && state == "Complete" {
                self.delegate?.enterBiometricComplite!(self)
                //                self.goToTouchAndFaceVC()
                print(state)
                print(verifier)
            } else {
                print(error!)
            }
        })
    }
}

//MARK:- UICollectionViewDataSource
extension EnterTouchView:UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierEnterTouchFaceCell, for: indexPath) as! EnterTouchFaceCell

        if currentBiometric != BiometricSupport.NoBiometric {
            cell.setupWithItem(biometric: currentBiometric!)
        }
        
        return cell
    }
}

//MARK:- UICollectionViewDelegate
extension EnterTouchView:UICollectionViewDelegate
{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        authenticateUser()
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        let contX = scrollView.contentOffset.x
//
//        let width = (collectionView.contentSize.width - collectionView.bounds.size.width);
//
//
//        if contX == width{
//
//            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
//                self.leftButton.alpha = 1.0
//                self.rightButton.alpha = 0.0
//            }) { (end) in
//
//            }
//
//        } else if contX == 0{
//
//            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
//                self.leftButton.alpha = 0.0
//                self.rightButton.alpha = 1.0
//            }) { (end) in
//
//            }
//        }
//    }
}


