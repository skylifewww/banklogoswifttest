//
//  ConfirmTransferVC.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 30.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import MDCSwipeToChoose
import DPLocalization


class ConfirmTransferVC: UIViewController {
    
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var denyButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var confirmTransferLabel: UILabel!
    @IBOutlet weak var confirmOkView: UIView!
    @IBOutlet weak var confirmOkImageView: UIImageView!
    @IBOutlet weak var confirmDenyImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    
    var isOk = false
    var originX:CGFloat = 0
    var originCenterX:CGFloat = 0
    var isFirstStepRight = false
    var isSecondStepRight = false
    var isThirdStepRight = false
    var isFirstStepLeft = false
    var isSecondStepLeft = false
    var isThirdStepLeft = false
    var transferView:TransferView?
    var guideView:GuideView?
    var enterTouchAndFaceView:EnterTouchAndFaceView?
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 12.0
    private var fillColor: UIColor = #colorLiteral(red: 0.2235294118, green: 0.2784313725, blue: 0.337254902, alpha: 1)
    var offsetX:CGFloat = 15.0
    var transfer:MDCSwipeToChooseView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let currentLanguage = UserDefaults.standard.string(forKey: "current_language")
        dp_set_current_language(currentLanguage)

        setupViews()
        setupTransferSwipes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isOk {
            self.confirmTransferLabel.text = DPLocalizedString("transfer_confirmed", nil)
        } else {
            self.confirmTransferLabel.text = DPLocalizedString("transfer_canceled", nil)
        }

    }
    
    private func setupTransferSwipes(){
        let options = MDCSwipeToChooseViewOptions()
        options.delegate = self
        
        colorView.setRoundedNoClipsToBoundsView(cornerRadius: 12)
        transfer = MDCSwipeToChooseView(frame: colorView.bounds, options: options)
        transferView = TransferView()
        self.colorView.addSubview(transfer!)
        self.colorView.layoutIfNeeded()
        originX = confirmOkView.frame.origin.x
        originCenterX = confirmOkView.frame.midX

        
        transfer.translatesAutoresizingMaskIntoConstraints = false
        transfer.topAnchor.constraint(equalTo: colorView.topAnchor).isActive = true
        transfer.leftAnchor.constraint(equalTo: colorView.leftAnchor).isActive = true
        transfer.bottomAnchor.constraint(equalTo: colorView.bottomAnchor).isActive = true
        transfer.rightAnchor.constraint(equalTo: colorView.rightAnchor).isActive = true
        transfer?.setRoundBorderEdgeView(cornerRadius: 12, borderWidth: 1, borderColor: .clear)
        transfer.layoutIfNeeded()
        transfer?.addSubview(transferView!)
        transferView?.frame = (transfer?.bounds)!
        transferView?.layoutIfNeeded()
        transferView?.drawDottedLineWithLabel()
        transferView?.setRoundedView(cornerRadius: 12)

        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.topAnchor.constraint(equalTo: colorView.topAnchor).isActive = true
        shadowView.leftAnchor.constraint(equalTo: colorView.leftAnchor).isActive = true
        shadowView.bottomAnchor.constraint(equalTo: colorView.bottomAnchor).isActive = true
        shadowView.rightAnchor.constraint(equalTo: colorView.rightAnchor).isActive = true
        shadowView.layoutIfNeeded()
        shadowView.setRoundedShadowView(cornerRadius: 12, color: fillColor)

        options.onPan = { state -> Void in
            
            if state?.direction == .left {
                self.confirmDenyImageView.isHidden = false
                self.confirmOkImageView.isHidden = true
                self.shadowView.frame = self.transfer.frame
                self.shadowView.center.x = self.transfer.center.x + self.colorView.frame.origin.x
                self.shadowView.center.y = self.colorView.center.y
                self.colorView.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
                
                let a = (self.colorView.frame.maxX - self.transfer.frame.maxX) / 2
                let a100 = a/1000 + 1.0
                let b = self.colorView.frame.maxX - 15
                let c = b - a
                let offsetX = c / a100

                if offsetX > self.colorView.frame.maxX - 20 - self.colorView.frame.minX {

                    UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        self.confirmOkView.isHidden = true
                    }) { (end) in
                        
                    }
                    
                    self.confirmOkView.isHidden = true
                    self.isSecondStepLeft = false
                    if self.isFirstStepLeft {
                        self.isFirstStepLeft = false
                        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.transform = CGAffineTransform(scaleX: 72.0/72.0, y: 72.0/72.0)
                            self.okButton.transform = CGAffineTransform(scaleX: 72.0/72.0, y: 72.0/72.0)
                        }) { (end) in
                            
                        }
                    }
                }
                
                if offsetX <= self.colorView.frame.maxX - 25 - self.colorView.frame.minX  && offsetX > self.colorView.frame.maxX - 45 - self.colorView.frame.minX{

                    UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        self.confirmOkView.isHidden = false
                    }) { (end) in
                        
                    }
                    self.confirmOkView.center.x = offsetX
                    self.isSecondStepLeft = false
                    if !self.isFirstStepLeft {
                        self.isFirstStepLeft = true
                        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.transform = CGAffineTransform(scaleX: 80.0/72.0, y: 80.0/72.0)
                            self.okButton.transform = CGAffineTransform(scaleX: 66.0/72.0, y: 66.0/72.0)
                            self.denyButton.alpha = 1.0
                            self.okButton.alpha = 1.0
                        }) { (end) in
                            
                        }
                    }
                }
                
                if offsetX <= self.colorView.frame.maxX - 45 - self.colorView.frame.minX && offsetX > self.colorView.frame.midX + self.confirmOkView.bounds.width/2 {
  
                    self.confirmOkView.isHidden = false
                    self.confirmOkView.center.x = offsetX
                    self.isFirstStepLeft = false
                    self.isThirdStepLeft = false
                    if !self.isSecondStepLeft {
                        self.isSecondStepLeft = true
                        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.alpha = 0.5
                            self.okButton.alpha = 0.5
                            self.denyButton.transform = CGAffineTransform(scaleX: 88.0/72.0, y: 88.0/72.0)
                            
                        }) { (end) in
                            
                        }
                    }
                }
                
                if offsetX <= self.colorView.frame.midX + self.confirmOkView.bounds.width/2{
                    
                    self.confirmOkView.isHidden = false
                    
                    if offsetX > self.originCenterX{
                        
                        self.confirmOkView.center.x = offsetX
                    } else {
                        
                        self.confirmOkView.center.x = self.originCenterX
                    }
                    
                    self.isFirstStepLeft = false
                    self.isSecondStepLeft = false
                    if !self.isThirdStepLeft {
                        self.isThirdStepLeft = true
                        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.alpha = 0.0
                            self.okButton.alpha = 0.0
                            
                        }) { (end) in
                            
                        }
                    }
                }
                
            } else if state?.direction == .right {
                self.confirmDenyImageView.isHidden = true
                self.confirmOkImageView.isHidden = false
                self.shadowView.frame = self.transfer.frame
                self.shadowView.center.x = self.transfer.center.x + self.colorView.frame.origin.x
                self.shadowView.center.y = self.colorView.center.y
                self.colorView.backgroundColor = #colorLiteral(red: 0.5058823529, green: 0.7725490196, blue: 0.3176470588, alpha: 1)
                
                let a = ((self.transfer.frame.minX - self.colorView.frame.minX) / 2 )
                let b = self.colorView.frame.minX + 15
                let c = a + b
                let offsetX = c * a/100

                if offsetX < self.colorView.frame.minX {

                    self.isSecondStepRight = false
                    UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        self.confirmOkView.isHidden = true
                    }) { (end) in
                        
                    }
                    if self.isFirstStepRight {
                        self.isFirstStepRight = false
                        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.transform = CGAffineTransform(scaleX: 72.0/72.0, y: 72.0/72.0)
                            self.okButton.transform = CGAffineTransform(scaleX: 72.0/72.0, y: 72.0/72.0)
                        }) { (end) in
                            
                        }
                    }
                }
                
                if offsetX >= self.colorView.frame.minX && offsetX < self.colorView.frame.minX + 35 {
                    UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        self.confirmOkView.isHidden = false
                    }) { (end) in
                        
                    }
                    self.confirmOkView.center.x = offsetX

                    self.isSecondStepRight = false
                    if !self.isFirstStepRight {
                        self.isFirstStepRight = true
                        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.transform = CGAffineTransform(scaleX: 66.0/72.0, y: 66.0/72.0)
                            self.okButton.transform = CGAffineTransform(scaleX: 80.0/72.0, y: 80.0/72.0)
                            self.denyButton.alpha = 1.0
                            self.okButton.alpha = 1.0
                        }) { (end) in
                            
                        }
                    }
                }
                
                if offsetX >= self.colorView.frame.minX + 35 && offsetX < self.colorView.frame.midX - self.confirmOkView.bounds.width/2{

                    self.confirmOkView.center.x = offsetX

                    self.isThirdStepRight = false
                    self.isFirstStepRight = false
                    if !self.isSecondStepRight {
                        self.isSecondStepRight = true
                        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.alpha = 0.5
                            self.okButton.alpha = 0.5
                            self.okButton.transform = CGAffineTransform(scaleX: 88.0/72.0, y: 88.0/72.0)
                            
                        }) { (end) in
                            
                        }
                    }
                }
                
                if offsetX >= self.colorView.frame.midX - self.confirmOkView.bounds.width/2{
                    if offsetX < self.originCenterX{

                        self.confirmOkView.center.x = offsetX
                    } else {

                        self.confirmOkView.center.x = self.originCenterX
                    }

                    self.isFirstStepRight = false
                    self.isSecondStepRight = false
                    if !self.isThirdStepRight {
                        self.isThirdStepRight = true
                        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.denyButton.alpha = 0.0
                            self.okButton.alpha = 0.0

                        }) { (end) in
                            
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: - IBAction metods
    
    @IBAction func settingsButtonTapped(_ sender: UIButton) {
        goToSettingsVC()
    }
    
    @IBAction func denyButtonTapped(_ sender: UIButton) {
        self.colorView.backgroundColor = #colorLiteral(red: 1, green: 0.1607843137, blue: 0.262745098, alpha: 1)
        confirmOkImageView.isHidden = true
        confirmDenyImageView.isHidden = false
        self.confirmOkView.setRoundBorderEdgeView(cornerRadius: 36, borderWidth: 1, borderColor: UIColor.white)
        self.startAnimationSettingsButtons()
        self.shadowView.removeFromSuperview()
        nopeFrontCardView()
    }
    @IBAction func okButtonTapped(_ sender: UIButton) {
        self.colorView.backgroundColor = #colorLiteral(red: 0.5058823529, green: 0.7725490196, blue: 0.3176470588, alpha: 1)
        confirmOkImageView.isHidden = false
        confirmDenyImageView.isHidden = true
        self.confirmOkView.setRoundBorderEdgeView(cornerRadius: 36, borderWidth: 1, borderColor: UIColor.white)
        self.startAnimationSettingsButtons()
        self.shadowView.removeFromSuperview()
        likeFrontCardView()
    }
    func nopeFrontCardView() -> Void{
        self.transferView?.mdc_swipe(MDCSwipeDirection.left)
        isOk = false
       finalAnimation(isOk: isOk)
    }
    func likeFrontCardView() -> Void{
        self.transferView?.mdc_swipe(MDCSwipeDirection.right)
        isOk = true
        finalAnimation(isOk: isOk)
    }
    
    private func finalAnimation(isOk: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations: {
            
            self.confirmOkView.transform = CGAffineTransform(translationX: 0, y: -17)
            self.confirmOkView.setRoundBorderEdgeView(cornerRadius: 36, borderWidth: 1, borderColor: UIColor.white)
            
            self.confirmTransferLabel.transform = CGAffineTransform(translationX: 0, y: -17)
            self.confirmTransferLabel.alpha = 1
            if isOk {
                self.confirmTransferLabel.text = DPLocalizedString("transfer_confirmed", nil)
            } else {
                self.confirmTransferLabel.text = DPLocalizedString("transfer_canceled", nil)
            }
            
        }) { (_) in
            
        }
    }
    
    private func startAnimationSettingsButtons() {

                self.denyButton.alpha = 0.0
                self.okButton.alpha = 0.0
                self.denyButton.isEnabled = false
                self.okButton.isEnabled = false
    }
    
    private func goToSettingsVC() {
        
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.presentDetail(settingsVC)
    }
    
    //MARK:- Setup Views Metods
    private func setupViews() {
        
//        if (Device.IS_IPHONE_5){
//
//            print("Device.IS_IPHONE_5")
//            numPadBottom.constant = 0
//            numPadHeight.constant = 240
//
//        } else if (Device.IS_IPHONE_X){
//            numPadBottom.constant = 16
//        } else {
//            numPadBottom.constant = -6
//        }

        
        //MARK:- Setup Views
        
//        isConfirmLabel.text = DPLocalizedString("сonfirm_transfer", nil)
//        self.autolocalizationKey = "languageKey"

        confirmTransferLabel.alpha = 0
        denyButton.setRoundButton(cornerRadius: denyButton.bounds.width/2)
        okButton.setRoundButton(cornerRadius: okButton.bounds.width/2)
        let numberTelStr = "+38(050)1234567"
        transferView?.setTextForTransferByNumberLabel(numberTelStr: numberTelStr)
        prepareEnterView()
        
//        prepareGuideOverView()
        
    }
    
   
    
    private func prepareEnterView(){

        enterTouchAndFaceView = EnterTouchAndFaceView()
        self.view.addSubview(enterTouchAndFaceView!)
        enterTouchAndFaceView?.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        enterTouchAndFaceView?.layoutIfNeeded()
    }
    
    private func prepareGuideOverView(){
        
        guideView = GuideView()
        self.view.addSubview(guideView!)
        guideView?.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        guideView?.overTransferView.setAnchor(top: colorView.topAnchor, left: colorView.leftAnchor, bottom: colorView.bottomAnchor, right: colorView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
   
        
        guideView?.overDenyButton.translatesAutoresizingMaskIntoConstraints = false
        guideView?.overDenyButton.centerXAnchor.constraint(equalTo: denyButton.centerXAnchor).isActive = true
        guideView?.overDenyButton.centerYAnchor.constraint(equalTo: denyButton.centerYAnchor).isActive = true
        guideView?.overDenyButton.heightAnchor.constraint(equalToConstant: 84).isActive = true
        guideView?.overDenyButton.widthAnchor.constraint(equalToConstant: 84).isActive = true
        
        guideView?.overOkButton.translatesAutoresizingMaskIntoConstraints = false
        guideView?.overOkButton.centerXAnchor.constraint(equalTo: okButton.centerXAnchor).isActive = true
        guideView?.overOkButton.centerYAnchor.constraint(equalTo: okButton.centerYAnchor).isActive = true
        guideView?.overOkButton.heightAnchor.constraint(equalToConstant: 84).isActive = true
        guideView?.overOkButton.widthAnchor.constraint(equalToConstant: 84).isActive = true
        guideView?.overTransferView.layoutIfNeeded()
        guideView?.overTransferView.drawDottedRoundedView(cornerRadius: 12, strokeColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), lineWidth: 2.0, lineDashPattern: [5, 4])
        guideView?.overTransferView.drawDottedLine(start: CGPoint(x: (guideView?.overTransferView?.bounds.midX)!, y: ((guideView?.overTransferView?.bounds.minY)! + 5.0)), end: CGPoint(x: (guideView?.overTransferView?.bounds.midX)!, y: ((guideView?.overTransferView?.bounds.maxY)! - 5.0)), strokeColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), lineWidth: 2.0, lineDashPattern: [5, 4])
        guideView?.overDenyButton.drawDottedRoundedView(cornerRadius: (guideView?.overDenyButton.bounds.width)!/2, strokeColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), lineWidth: 2.0, lineDashPattern: [5, 4])
        guideView?.overOkButton.drawDottedRoundedView(cornerRadius: (guideView?.overOkButton.bounds.width)!/2, strokeColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), lineWidth: 2.0, lineDashPattern: [5, 4])
        
        let removeOverViewButton = UIButton()
        guideView?.addSubview(removeOverViewButton)
        removeOverViewButton.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        removeOverViewButton.addTarget(self, action:  #selector(removeOverViewButtonTapped(_:)), for: .touchUpInside)
        
    }
    
    @objc func removeOverViewButtonTapped(_ sender:UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            self.guideView?.alpha = 0.0
  
        }) { (_) in
            self.guideView?.removeFromSuperview()
            self.self.guideView = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - MDCSwipeToChooseDelegate metods
extension ConfirmTransferVC: MDCSwipeToChooseDelegate {
    
    // This is called when a user didn't fully swipe left or right.
    func viewDidCancelSwipe(_ view: UIView) -> Void{

        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.denyButton.transform = CGAffineTransform(scaleX: 72.0/72.0, y: 72.0/72.0)
            self.okButton.transform = CGAffineTransform(scaleX: 72.0/72.0, y: 72.0/72.0)
        }) { (end) in
            
        }
        self.shadowView.center.x = self.transfer.center.x + self.colorView.frame.origin.x
        self.shadowView.center.y = self.colorView.center.y
    }
    
    // Sent before a choice is made. Cancel the choice by returning `false`. Otherwise return `true`.
    func view(_ view: UIView, shouldBeChosenWith: MDCSwipeDirection) -> Bool {
        if shouldBeChosenWith == .left {
            
            self.startAnimationSettingsButtons()
            self.shadowView.removeFromSuperview()
            return true
        } else {

            self.startAnimationSettingsButtons()
            self.shadowView.removeFromSuperview()
            return true
        }
    }
    
    // This is called when a user swipes the view fully left or right.
    func view(_ view: UIView, wasChosenWith: MDCSwipeDirection) -> Void {

        if wasChosenWith == .left {

            confirmOkImageView.isHidden = true
            confirmDenyImageView.isHidden = false
            self.confirmOkView.setRoundBorderEdgeView(cornerRadius: 36, borderWidth: 1, borderColor: UIColor.clear)
            isOk = false
            self.finalAnimation(isOk: isOk)
            
        } else if wasChosenWith == .right {

            confirmOkImageView.isHidden = false
            confirmDenyImageView.isHidden = true

            self.confirmOkView.setRoundBorderEdgeView(cornerRadius: 36, borderWidth: 1, borderColor: UIColor.clear)
            
            isOk = true

            self.finalAnimation(isOk: isOk)
        }
    }
}
