//
//  NetworkManager.swift
//  BankLogoSwiftTest
//
//  Created by Vladimir Nybozhinsky on 20.07.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SDWebImage

struct Post: Codable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}

class NetworkManager
    
{
    fileprivate var alamofireManager: Alamofire.SessionManager!
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30 // seconds
        configuration.timeoutIntervalForResource = 30 // seconds
        
      
        
        alamofireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    public func postRegisterRequest(cellPhone:String, completionHandler:@escaping (_ confirmationToken: String, _ error: String?) -> Void) {
        
        let body: NSMutableDictionary? = [
            
            "cell_phone": cellPhone,
            "channel": "Mobile"]
        
        let url = URL(string: Constants.registrationURL)
        print(Constants.registrationURL)
        
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: body!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        print(request)
        let alamoRequest = Alamofire.request(request as URLRequestConvertible)
        //        alamoRequest.validate(statusCode: 200..<300)
        alamoRequest.responseJSON(completionHandler: { response in
            
            switch response.result {
            case .success(let result):
                let resultJSON = JSON.init(result)
                print(resultJSON)
                if let confirmationJSON = resultJSON.dictionary {
                    if let confirmation = confirmationJSON["confirmation_token"]?.string{
                        completionHandler(confirmation, nil)
                    }
                }
            case .failure(let error):
                completionHandler("", error.localizedDescription)
                print(error)
                print(response)
            }
        })
    }
    
    public func putRegisterRequest(cellPhone:String, confirmationToken:String, smsCode:String, completionHandler:@escaping (_ state: String, _ verifier: String, _ error: String?) -> Void) {
        let body: NSMutableDictionary? = [
            "cell_phone": cellPhone,
            "channel": "Mobile"]
        
        let url = URL(string: Constants.registrationURL)
        print(Constants.registrationURL)
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "PUT"
        
        request.addValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        request.addValue(smsCode, forHTTPHeaderField: "X-Confirmation-Code")
        request.addValue(confirmationToken, forHTTPHeaderField: "X-Confirmation-Token")
        let data = try! JSONSerialization.data(withJSONObject: body!, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
   
        let alamoRequest = Alamofire.request(request as URLRequestConvertible)

        alamoRequest.responseJSON(completionHandler: { response in

            switch response.result {
            case .success(let result):
    
                let resultJSON = JSON.init(result)
                print("resultJSON \(resultJSON)")
                if let confirmationJSON = resultJSON.dictionary {
                    if let state = confirmationJSON["state"]?.string{
//                        if let verifier = confirmationJSON["verifier"]?.string{
                            completionHandler(state, "", nil)
//                        completionHandler(state, verifier, nil)
//                        }
                    } else if let errorType = confirmationJSON["type"]?.string{
                            completionHandler("", "", errorType)
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
                completionHandler("", "", error.localizedDescription)
            }
        })
    }
    
    public func deleteIdentitiesRequest(completionHandler:@escaping (_ clientMessage:String, _ error: String?) -> Void) {
        
        let url = URL(string: Constants.identitiesURL)
        print(Constants.identitiesURL)
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let request = alamofireManager.request(url!, method: .delete, headers: headers).responseJSON(completionHandler: { response in
            print(response.result.value)
            
            switch response.result {
            case .success(let result):
                print(response.request)
                print(response.request?.httpBody)
                let resultJSON = JSON.init(result)
                print(resultJSON)
                
                if let serverKeyJSON = resultJSON.dictionary {
                    if let clientMessage = serverKeyJSON["client_message"]?.string{
                        
                        if clientMessage == "Authorization is required to complete operation"{
                            print(clientMessage)
                            completionHandler(clientMessage, nil)
                        }
                    }
                }
            case .failure(let error):
                completionHandler( "", error.localizedDescription)
                print(error)
            }
        })
        print("postIdentitiesRequest: \(request)")
    }
    
    public func postIdentitiesRequest(clientOpenKey:String, confirmationToken:String, completionHandler:@escaping (_ login: String, _ serverOpenKey: String, _ error: String?) -> Void) {

        let body: [String: Any]? =
            [
                "auth_type": "Device",
                "group_id": "Curve25519",
                "client_open_key": clientOpenKey,
                "device": [
                    "device_token": [
                        "token": "d5GVIe1kxSU:APA91bFrPoJBl-jXxhwYb3Tg2YlvN67mUgxHmGeyadNX_Odwf3H_zTcxdd7CjH5KkcOl0ui7FTAJRcPZMbxAeSQTeeTCf2YTFOB63SX2LHmqTnZj88hgH1YxDp0Xfiwod18lqz5TfHiMbVbPTHcDjC3IGo78r-ftpB",
                        "type_id": "Firebase"
                    ],
                    "imei": "",
                    "key": "7976def9-c1ac-4eb0-915a-af7c3b230061",
                    "name": "ANE-LX1",
                    "os": [
                        "name": "8.0.0",
                        "type": "iOS",
                        "version": "11.4"
                    ],
                    "rooted": false,
                    "vendor": "Apple",
                    "version": "iPhoneX"
                ],
            ]
        
 
        let url = URL(string: Constants.identitiesURL)
        print(Constants.identitiesURL)
        
        self.setCookies(confirmationToken: confirmationToken, url: url!)

        let headers: HTTPHeaders = [
            "Content-Type":"application/json-patch+json"
        ]
        
      let request = alamofireManager.request(url!, method: .post, parameters: body, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON(completionHandler: { response in
        print(response.result.value)

            switch response.result {
            case .success(let result):
                print(response.request)
                print(response.request?.httpBody)
                let resultJSON = JSON.init(result)
                print(resultJSON)

                if let serverKeyJSON = resultJSON.dictionary {
                    if let login = serverKeyJSON["login"]?.string{
                        if let serverOpenKey = serverKeyJSON["server_open_key"]?.string{
                           completionHandler(login, serverOpenKey, nil)
                        }
                    }
                }
            case .failure(let error):
                completionHandler("", "", error.localizedDescription)
                print(error)
            }
        })
        print("postIdentitiesRequest: \(request)")
    }
    
    public func postScrumLoginRequest(clientNonce: String, clientProof: String, login: String, serverNonce: String, confirmationToken:String, completionHandler:@escaping (_ state: String, _ verifier: String, _ error: String?) -> Void) {
        
        let body: [String: Any]? = [
            "auth_type": "Device",
            "channel": "Mobile",
            "client_nonce": clientNonce,
            "client_proof": clientProof,
            "login": login,
            "server_nonce": serverNonce]
        
        
        let url = URL(string: Constants.logonScramURL)
        print(Constants.logonScramURL)
        
        self.setCookies(confirmationToken: confirmationToken, url: url!)

        let headers: HTTPHeaders = [
            "Content-Type":"application/json-patch+json"
        ]

        
       let request = alamofireManager.request(url!, method: .post, parameters: body, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON(completionHandler: { response in

            print(response)
        print(response.result.value)
            switch response.result {
                
            case .success(let result):
                print(response.request)
                print(response.request?.httpBody)
//                print(result)
                let resultJSON = JSON.init(result)
                print("postScrumLoginResponse: \(resultJSON)")
                                if let confirmationJSON = resultJSON.dictionary {
                                    if let state = confirmationJSON["state"]?.string{
//                                        if let verifier = confirmationJSON["verifier"]?.string{
                                            completionHandler(state, "", nil)
//                                        completionHandler(state, verifier, nil)
//                                        }
                                    }
                            }
            case .failure(let error):
                print(error)
                completionHandler("", "", error.localizedDescription)
            }
        })
        print("postScrumLoginRequest: \(request)")
    }
    
    public func getScrumDeviceRequest(confirmationToken:String, login: String, completionHandler:@escaping (_ serverNonce: String, _ salt: String, _ iterations: Int, _ error: String?) -> Void) {
        
        let url = URL(string: Constants.logonDeviceURL + login)
        print(Constants.logonDeviceURL + login)
        
        if confirmationToken.count > 0 {
            self.setCookies(confirmationToken: confirmationToken, url: url!)
        }
        
        
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "GET"
        request.setValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        
        let alamoRequest = Alamofire.request(request as URLRequestConvertible)
        //        alamoRequest.validate(statusCode: 200..<300)
        alamoRequest.responseJSON(completionHandler: { response in
            print(response)
            switch response.result {
                
            case .success(let result):
//                print(result)
                let resultJSON = JSON.init(result)
                print(resultJSON)
                if let logonJSON = resultJSON.dictionary {
                    if let serverNonce = logonJSON["server_nonce"]?.string{
                        if let salt = logonJSON["salt"]?.string{
                            if let iterations = logonJSON["iterations"]?.int{
                                completionHandler(serverNonce, salt, iterations, nil)

                            }
                        }
                    }
                }
            case .failure(let error):
                print(error)
                completionHandler("", "", 0, error.localizedDescription)
            }
        })
    }
    
    public func logoutRequest() {
        
        let url = URL(string: Constants.logOutURL)
        print(Constants.logOutURL)
//        self.setCookies(confirmationToken: confirmationToken, url: url!)
        
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "DELETE"
        request.setValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        
        let alamoRequest = Alamofire.request(request as URLRequestConvertible)
        //        alamoRequest.validate(statusCode: 200..<300)
        alamoRequest.responseJSON(completionHandler: { response in
            print(response)
            switch response.result {
                
            case .success(let result):
                //                print(result)
                let resultJSON = JSON.init(result)
                                print(resultJSON)
//                if let logonJSON = resultJSON.dictionary {
//                    if let serverNonce = logonJSON["server_nonce"]?.string{
//                        if let salt = logonJSON["salt"]?.string{
//                            if let iterations = logonJSON["iterations"]?.int{
////                                completionHandler(serverNonce, salt, iterations, nil)
//
//                            }
//                        }
//                    }
//                }
            case .failure(let error):
                print(error)
//                completionHandler("", "", 0, error.localizedDescription)
            }
        })
    }
    
    private func setCookies(confirmationToken:String, url :URL) {
        let httpCookie = HTTPCookie.init(properties:
            [HTTPCookiePropertyKey.version : "0",
             HTTPCookiePropertyKey.name : "Authorization",
             HTTPCookiePropertyKey.value : confirmationToken,
             HTTPCookiePropertyKey.expires : "2027-05-13 09:21:23 +0000"])
        
        if httpCookie != nil, let cookieStorage = Alamofire.SessionManager.default.session.configuration.httpCookieStorage {
            cookieStorage.setCookies([httpCookie!], for: url, mainDocumentURL: nil)
        }
    }

}
